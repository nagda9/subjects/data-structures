#ifndef TAXI_H
#define TAXI_H

#include "Car.h"

using namespace std;

class Taxi: public Car {

protected:

	//uj valtozok

	double pocket;
	double kmhCost;

public:

	//konstruktor

	Taxi() {
	}

	Taxi(string type, double consumption, int capacity, double kmCost) :
			Car(type, consumption, capacity), kmhCost(kmCost) {
		pocket = 0;
	}

	//feluldefinialo fuggvenyek

	virtual double cost(double km) {
		return Car::cost(km) + kmhCost * km;
	}
	// a Taxi::refuel(double) felüldefiniálná a Car::refuel(double) metódust

	// De! itt elfedés fog történni!
	// a Taxi::refuel(int) elfedi a Car::refuel(double) metódust
	virtual void refuel(int litre) {
		petrol += litre;
		pocket -= litre * petrolCost;
	}

	//uj fuggvenyek

	double carriage(double km) {
		if (moveOn(km) == km) {
			pocket += cost(km);
			return km;
		} else
			return 0;
	}

	//tulterheles

	double memberCost(double km, int member) {
		if (member > capacity + 1) {
			return 0;
		}
		return cost(km) / member;
	}

	// uj getterek

	double getKmCost() {
		return kmhCost;
	}

	double getPocket() {
		return pocket;
	}

};

#endif /*TAXI_H*/
