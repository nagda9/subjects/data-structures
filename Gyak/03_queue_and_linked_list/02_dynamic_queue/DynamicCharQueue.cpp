#include "DynamicCharQueue.h"
#include "../exceptions/exceptions.hpp"

#include <iostream>

DynamicCharQueue::~DynamicCharQueue() {
  while (!isEmpty()) {
    out();
  }
}

bool DynamicCharQueue::isEmpty() const { return head == nullptr; }

void DynamicCharQueue::in(char new_item) {
  Node *p = new Node(new_item);
  if (isEmpty()) {
    tail = p;
    head = p;
  } else {
    tail->pNext = p;
    tail = p;
  }
}

char DynamicCharQueue::out() {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  int tmp = head->value;
  Node *p = head;
  head = head->pNext;
  delete p;
  return tmp;
}

char DynamicCharQueue::first() const {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  return head->value;
}

void DynamicCharQueue::print() const {
  for (Node *i = head; i != nullptr; i = i->pNext) {
    std::cout << i->value << " ";
  }
}
