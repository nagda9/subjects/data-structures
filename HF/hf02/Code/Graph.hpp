#ifndef HF02_GRAPH_HPP
#define HF02_GRAPH_HPP

#include <iostream>
#include <utility>
#include <vector>
#include <unordered_map>
#include "Exceptions.hpp"
#include <fstream>
#include <sstream>
#include <filesystem>
#include <queue>
#include <list>
#include <map>
#include <unordered_set>

using namespace std;

class Graph { // összefüggő, élsúlyozott, egyszerű gráf, nincs hurokél, nincs többszörös él, nincs irányítottság
public:
    struct Edge {
        string vertex1{}, vertex2{};
        int val{};

        Edge() = default;
        ~Edge() = default;
        explicit Edge(string v1, string v2, int value) : vertex1(std::move(v1)), vertex2(std::move(v2)), val(value) {};
        bool operator==(const Edge &) const;
        bool operator<(const Edge &other) const;
    };

    struct edge_hasher {
        size_t operator()(const Edge &e) const {
            size_t h1 = hash<string>{}(e.vertex1);
            size_t h2 = hash<string>{}(e.vertex2);
            size_t h3 = hash<int>{}(e.val);
            return h1 ^ h2 ^ h3;
        }
    };

    struct Node {
        string key{};
        unordered_map<string, Edge *> edges{};

        Node() = delete;
        ~Node() = default;
        explicit Node(string name) : key(std::move(name)) {};
    };

private:
    unordered_map<string, Node *> vertices{};
    unordered_map<Edge, Edge *, edge_hasher> edges;
public:
    Graph() = default;
    ~Graph();
    Graph(const Graph &);
    Graph &operator=(const Graph &);
    Graph(const Graph &&) = delete;
    Graph &operator=(const Graph &&) = delete;

    [[maybe_unused]] void readGraph(const std::string &);

    [[maybe_unused]] void exportGraph(const string &filename);

    void addEdge(const string &from, const string &to, int val);

    [[maybe_unused]] void removeEdge(const string& from, const string& to);

    [[maybe_unused]] void addVertex(const string &vertex, const vector<pair<string, int>> &edges);

    void clear();


    void removeVertex(const string& vertex);

    bool is_connected();

    [[maybe_unused]] void print();

    unordered_map<string, unordered_map<string, bool>> getAdjacencyMatrix();
    [[maybe_unused]] void printAdjacencyMatrix();

    Graph PrimMST(); // Prim's Minimum Spanning Tree
    [[maybe_unused]] void printPrimMST();

    map<string, list<string>> DijsktraSP(const string &source); // Dijkstra's Shortest Path
    [[maybe_unused]] void printDijsktraSP(const string &source);
};


#endif //HF02_GRAPH_HPP
