#ifndef HASHTABLE_HPP_INCLUDED
#define HASHTABLE_HPP_INCLUDED

#include <iostream>
#include <list>
#include <vector>

// Intből int ujjlenyomatot képező funktor
class IntFingerprint {
public:
  int operator()(int key) { return key; }
};

// Stringből int ujjlenyomatot képező funktor
class StringFingerprint {
public:
  int operator()(const std::string &key) {
    unsigned h = 0;
    for (char i : key) {
      h += (256 * h + i) % 16777213;
    }
    return int(h);
  }
};

// Telefonszámot tartalmazó stringből int ujjlenyomatot képező funktor
// Utolsó 9 számjegy (az mindig belefér 32 bitbe)
class PhoneNumberFingerprint {
public:
  int operator()(const std::string &key) {
    int sum = 0;
    for (int i = std::max(0, (int)(key.size() - 9)); i < (int)key.size(); ++i) {
      sum = sum * 10 + (key[i] - '0');
    }
    return sum;
  }
};

// HashTable osztály
// Halmazként működik - minden elemet csak egyszer tárolunk el benne
// Két template paraméter:
//   Key: kulcs típusa
//   Fingerprint: ujjlenyomatfunktor
template <class Key, class Fingerprint> class HashTable {
  std::vector<std::list<Key>> table; // A hashtábla tárterülete
  int capacity; // Hashtábla kapacitása (különböző hashek száma)
                // Nem egyenlő a maximális mérettel, tetszőleges mennyiségű
                // elemet el tudunk tárolni!
  int actualsize;          // Hashtáblában éppen megtalálható elemek száma
                           // Nem egyenlő a kapacitással!
  Fingerprint fingerprint; // Ujjlenyomatfunktor egy példánya (funktor = függvény osztály, a fingerprint függvényként működik, mivel a Fingerprint functornak van fv hívás operátora)

public:
  // Konstruktor, a hashtábla kapacitását kell átadni neki
  explicit HashTable(int capacity)
      : table(capacity), // Létrehozza az adott méretű vektort a hashtáblához
        capacity(capacity), actualsize(0) {}

  // Visszaadja a hashtáblában található elemek számát
  int size() { return actualsize; }

  // Visszaadja, hogy üres-e a hastábla
  bool empty() { return actualsize == 0; }

  // Kitörli az összes elemet a hashtáblából
  void clear() {
    for (int i = 0; i < capacity; ++i) {
      table[i].clear();
    }

    actualsize = 0;
  }

  // Új elemet szúr be a hashtáblába, ha nincs még ilyen elem benne
  // Ha van már ilyen elem, nem csinál semmit
  void insert(const Key &key) {
    int myHash = getHash(key);
    for (typename std::list<Key>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        // Már szerepel az elem, nem csinálunk semmit
        return;
      }
    }
    table[myHash].push_back(key);
    ++actualsize;
  }

  // Megkeresi, hogy egy adott elem megtalálható-e a hashtáblában
  bool find(const Key &key) {
    int myHash = getHash(key);
    for (typename std::list<Key>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        return true;
      }
    }
    return false;
  }

  // Kitöröl egy adott elemet, ha megtalálható a hashtáblában
  // Ha nem található meg, nem csinál semmit
  void erase(const Key &key) {
    int myHash = getHash(key);
    for (typename std::list<Key>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        table[myHash].erase(it);
        --actualsize;
        return;
      }
    }
  }

  // Kiírja a hashtábla tartalmát
  // Soronként egy hashérték, majd szóközzel elválasztva a benne található
  // értékek
  void print() {
    for (int i = 0; i < capacity; ++i) {
      std::cout << i << ": ";
      for (typename std::list<Key>::iterator it = table[i].begin();
           it != table[i].end(); ++it) {
        std::cout << *it << " ";
      }
      std::cout << std::endl;
    }
  }

private:
  // Visszaadja egy adott kulcs hashét
  int getHash(const Key &key) {
    // Egyszerű hashfüggvény: csak visszaadja az ujjlenyomatot modulózva a
    // kapacitással
    return fingerprint(key) % capacity;
  }
};

#endif // HASHTABLE_HPP_INCLUDED
