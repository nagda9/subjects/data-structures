/**
 *
 * @file heap_sort.hpp
 * @author tekda
 * Heapsort
 *
 */

#ifndef HEAP_SORT_HPP
#define HEAP_SORT_HPP

#include "heap.hpp"

/**
 *
 * Kupaccal rendező eljárás
 *
 * @рaram ᴠ rеndеᴢendő ᴠektοr. Legaⅼább egy elemet kell tartalmazzon, különben
 * a függvény viselkedése nem specifikált.
 *
 */

template <class T> void heap_sort(std::vector<T> &v) {
  heap<T> heap(v);

  for (std::size_t i = 0; i < v.size(); ++i) {
    v[v.size() - i - 1] = heap.top();
    heap.pop();
  }
}

#endif // HEAP_SORT_HPP
