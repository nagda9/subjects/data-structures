#ifndef LIST_HPP_INCLUDED
#define LIST_HPP_INCLUDED

#include "../exceptions/listexception.hpp"
#include <iostream>

/**
 * Kétszeresen láncolt lista implementáció
 */

template <class T> /// Jelezzük, hogy ez egy "T" típusú templates osztály lesz
class List {
private:
  /** A szerkezetet felépítő belső osztály, kívülről nem látszik
   *
   * A láncoláshoz minden elem tartalmaz egy a következőre, és egy az
   * előzőre mutató pointert, valamint magát az adatot.
   */
  struct Node {
    T data; // Azt szeretnénk, hogy az eltárolt érték típusát megadhassuk
    Node *prev; // Az előző elemre mutató pointer
    Node *next; // A következő elemre mutató pointer

    // Konstruktor, ami beállít egy értéket, és lenullázza a mutatókat
    Node(T data0) : data(data0), prev(nullptr), next(nullptr) {}
    // Konstuktor, ami beállítja az értéket, és beállítja a mutatókat
    Node(T data0, Node *prev0, Node *next0)
        : data(data0), prev(prev0), next(next0) {}
  };
  Node *head; // A lista elejére mutató pointer
  Node *tail; // A lista végére mutató pointer
  Node *cur;  // A lista aktuális elemére mutató pointer

public:
  template <class U>
  friend std::ostream &operator<<(std::ostream &, const List<U> &);

  /**
   * Iterátor a listához - belső osztály
   *
   * Ez az osztály teszi lehetővé a listánk bejárását.
   */
  class Iterator {
    friend class List; // A listának hozzá kell férnie az iterátor privát
                       // adattagjához (pl. keresés esetén)
  private:
    Node *current; // Az aktuális listelem, amelyre az iterátor mutat

  public:
    // Néhány segédtípus, hogy a standard library-ben található függvények
    // jobban kezeljék az iterátorunkat.
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;
    // Az iterátor típusa
    using iterator_category = std::bidirectional_iterator_tag;

    Iterator() : current{nullptr} {}
    Iterator(Node *node) : current{node} {}
    // Iteratorok (és nem az értékeik!) összehasonlítása.
    bool operator!=(const Iterator &) const;
    bool operator==(const Iterator &) const;

    // Klasszikus iterátoraritmetika. Vegyük észre, hogy nagyon hasonló a
    // pointeréhez
    Iterator &operator++();   // Prefix következőre léptetés
    Iterator operator++(int); // Postfix következőre léptetés
    Iterator &operator--();   // Prefix előzőre léptetés
    Iterator operator--(int); // Postfix előzőre léptetés
    T &operator*() const;     // Dereferencia
  };

  List();
  List(const List<T> &list);
  List &operator=(const List<T> &list);
  ~List();

  /// Az aktuális (cur) elemet befolyásoló metódusok
  /// Nehézkesebb API, mint az iterátor, inkább az javasolt.
  void toFirst();
  void toLast();
  void stepNext();
  void stepPrev();
  T getValue() const;
  void setValue(T e);

  /// A lista állapotát lekérdező metódusok
  bool isEmpty() const; //Üres-e a lista?
  bool isLast() const;  // A lista aktuális mutatója az utolsó elemen áll-e?
  bool isFirst() const; // A lista aktuális mutatója az első helyen áll-e?
  bool isCurNull() const; // A lista aktuális mutatója mutat-e elemre?

  /// Beszúró metódusok
  void insertFirst(T e); // Új T típusú elem hozzáfűzése a lista elejéhez
  void insertLast(T e);  // Új T típusú elem hozzáfűzése a lista végéhez
  void insertBefore(T e); // T típusú elem beszúrása az aktuális elem elé
  void insertAfter(T e); // T típusú elem beszúrása az aktuális elem mögé

  /// Törlő metódusok
  // Az alábbi metódusok érvénytelenné teszik az adott elemre mutató
  // iterátorokat (azokat a továbbiakban használni undefined reference).
  void removeFirst(); // A lista első elemének törlése
  void removeLast();  // A lista utolsó elemének törlése
  void removeCur();   // Az aktuális elem törlése
  void clear();       // A lista törléséhez egy segéd metódus

  /// Kiegészítő eljárás
  void print() const; // kiíratás
  // Nem része a lista adatszerkezetnek, csak nekünk segítség!

  /// Iterátor függvények
  Iterator begin() const;
  Iterator end() const;  // utolsó elem utánra mutat
  Iterator last() const; // utolsó elemre mutat
  Iterator find(const T &e) const;
};

/// ----- Most következnek a metódus implementációk ----- ///

// Két iterátor akkor és csak akkor lesz egyenlő, ha ugyanazon a listán haladnak
// és ugyanarra a csúcsra mutatnak. Az alábbi műveletek mind két feltételt
// ellenőrzik Különbözőek-e az iterátorok?
template <class T>
bool List<T>::Iterator::operator!=(const Iterator &it) const {
  return current != it.current;
}

template <class T>
bool List<T>::Iterator::operator==(const Iterator &it) const {
  return current == it.current;
}

template <class T> typename List<T>::Iterator &List<T>::Iterator::operator++() {
  // Ha ez egy érvényes iterátor,
  if (!current) {
    throw InvalidIterator();
  }
  // akkor a következő elemre lépünk,
  current = current->next;
  // majd visszatérünk az objektumra mutató referenciával.
  return *this;
}

template <class T>
typename List<T>::Iterator List<T>::Iterator::operator++(int) {
  // Ha ez egy érvényes iterátor,
  if (!current) {
    throw InvalidIterator();
  }
  // akkor lemásoljuk (elmentjük a jelenlegi helyzetet),
  Iterator it(current);
  // tpvábblépünk egyel,
  current = current->next;
  // majd visszatérünk az ideiglenes, még az előző elemre mutató iterátorral.
  return it;
}

template <class T> typename List<T>::Iterator &List<T>::Iterator::operator--() {
  if (!current) {
    throw InvalidIterator();
  }

  current = current->prev;
  return *this;
}

template <class T>
typename List<T>::Iterator List<T>::Iterator::operator--(int) {
  if (!current) {
    throw InvalidIterator();
  }
  Iterator it(current);
  current = current->prev;
  return it;
}

template <class T> T &List<T>::Iterator::operator*() const {
  if (!current) {
    throw InvalidIterator();
  }
  return current->data;
}

// Alap konstruktor, üres listát hoz létre
template <class T> List<T>::List() {
  head = tail = cur = nullptr; // A mutatókat mind nullptr-ra állítjuk
}

// Másoló konstruktor: Új lista létrehozása egy már meglévő alapján. (klónozás)
// A másoláshoz át kell tenni az elemeket, egy másik memóriaterületre
template <class T> List<T>::List(const List<T> &list) {
  head = tail = cur = nullptr; // A mutatókat mind nullptr-ra állítjuk
  // Egy i pointer fut végig az elemeken, amíg a tail után ki nem fut a listából
  for (Node *i = list.head; i != nullptr; i = i->next)
    insertLast(i->data);
  cur = list.cur;
}

//Értékadó operátor: Az eredeti lista elemeit cseréli le
template <class T> List<T> &List<T>::operator=(const List<T> &list) {
  // Ha a kapott lista megegyezik az eredetivel, akkor készen vagyunk
  if (&list == this) {
    return *this;
  }
  // Egyébként ki kell ürítenünk a már meglévő listánkat, ha nem üres
  if (!isEmpty()) {
    clear();
  }
  for (Node *i = list.head; i != nullptr; i = i->next) {
    insertLast(i->data); // Sorban belemásoljuk az elemeket
  }
  cur = list.cur;
  return *this;
}

// Destructor
template <class T> List<T>::~List() { clear(); }

// A lista kiürítése
template <class T> void List<T>::clear() {
  // Ha üres a lista akkor nincsen dolgunk
  if (isEmpty()) {
    return;
  }
  for (Node *i = head; i != nullptr;
       /*A ciklus változó inkrementálása nem itt lesz*/) {
    Node *tmp = i;
    // Itt inkrementálunk, mivel most még meg van a törlendő elem, rá tudunk
    // lépni a rákövetkezőre
    i = i->next;
    delete tmp;
  }
  head = tail = cur = nullptr;
}

/// Az aktuális (cur) elemet befolyásoló metódusok implementációi
// Az aktuális elemet az elsõ lista elemre állítja
template <class T> void List<T>::toFirst() {
  cur = head; // Az aktuális mutasson az első elemre
}

// Az aktuális elemet az utolsó lista elemre állítja
template <class T> void List<T>::toLast() {
  cur = tail; // Az aktuálsi mutasson az utolsó elemre
}

// Az aktuális mutatót (ha nem nullptr az értéke) egy elemmel hátrébb állítja
// Vegyük észre, hogy ez az eljárás nullptr-á teheti az cur-ot! (Ha cur = tail
// volt)
template <class T> void List<T>::stepNext() {
  if (cur)           // Ha az aktuális mutató érvényes elemre mutat
    cur = cur->next; // akkor az aktuálist egyel hátrább léptetjük
}

// Az aktuális mutatót (ha nem nullptr az értéke) egy elemmel előrébb állítja
// Vegyük észre, hogy ez az eljárás nullptr-á teheti az cur-ot! (Ha cur = head
// volt)
template <class T> void List<T>::stepPrev() {
  if (cur)           // Ha az aktuális mutató érvényes elemre mutat
    cur = cur->prev; // akkor az aktuálist egyel előrébb léptetjük
}

// Visszaadja az aktuális elem értékét
template <class T> T List<T>::getValue() const {
  if (isEmpty())                 // Ha üres a lista
    throw(UnderFlowException()); // akkor egy UnderFlowException-t dobunk
  if (!cur) // Ha nem üres, mert nem dobtunk kivételt, de az cur = nullptr
    throw(CurNullException()); // akkor egy CurNullException-t dobunk
  return cur->data; // Ha minden rendbe, mert nem volt kivétel, akkor visszadjuk
                    // az aktuális elem értékét
}

// Beállítjuk az aktuális elem értékét
template <class T> void List<T>::setValue(T e) {
  if (isEmpty())                 // Ha üres a lista
    throw(UnderFlowException()); // akkor egy UnderFlowException-t dobunk
  if (!cur) // Ha nem üres, mert nem dobtunk kivételt, de az cur = nullptr
    throw(CurNullException()); // akkor egy CurNullExceptiont dobunk
  // Ha minden rendben, akkor be tudjuk állítani az aktuális elem értékét
  cur->data = e;
}

/// A lista állapotát lekérdező metódusok implementációi
// Megvizsgálja, hogy üres-e a lista (TRUE: ha üres, FALSE: ha nem üres)
template <class T> bool List<T>::isEmpty() const {
  return head == nullptr; // Visszaadjuk, hogy a head nullptr-e, mert az csak
                          // akkor nullptr, ha üres a lista.
}

// Megvizsgálja, hogy az aktuális elem-e az utolsó (TRUE: ha igen, FALSE: ha
// nem)
template <class T> bool List<T>::isLast() const { return cur == tail; }

// Megvizsgálja, hogy az aktuális elem-e az első (TRUE: ha igen, FALSE: ha nem)
template <class T> bool List<T>::isFirst() const { return cur == head; }

// Megvizsgáljuk, hogy a lista végén vagyunk-e (TRUE: ha igen, FALSE: ha nem)
template <class T> bool List<T>::isCurNull() const { return cur == nullptr; }

/// Beszúró metódusok implementálása
// A lista elejére szúrja be a paraméterben kapott új értéket
template <class T> void List<T>::insertFirst(T e) {
  Node *p = new Node(e); // Létrehozzuk az új elemet,

  // Ha üres a lista...
  if (isEmpty()) {
    //...akkor ez az új elem lesz az első, utolsó, és aktuális
    cur = head = tail = p;
  } else {
    // Ha már van elem a listában
    p->next = head; // Az új elem rákövetkezője lesz a régi, első elem
    head->prev = p; // A régi, első elem megelőzője lesz az új elem
    cur = head = p; // Az új elem lesz az első, és az aktuális is
  }
}

// A lista végére szúrja be a paraméterben kapott új értéket
template <class T> void List<T>::insertLast(T e) {
  Node *p = new Node(e); // Létrehozzuk az új elemet

  // Ha a lista üres...
  if (isEmpty()) {
    //...akkor ez az új elem lesz az első, utolsó, és aktuális
    cur = head = tail = p;
  } else {
    // Ha már van elem a listában
    p->prev = tail; // Az új elem megelőzője lesz a régi, utolsó elem
    tail->next = p; // A régi, utolsó elem rákövetkezője lesz az új elem
    cur = tail = p; // Az új elem lesz az utolsó, és az aktuális is
  }
}

// Az aktuális elem elé szúrja be a paraméterben kapott új értéket
template <class T> void List<T>::insertBefore(T e) {
  if (!cur)                    // Ha nincs aktuális elem...
    throw(CurNullException()); //...akkor CurNullException-t dobunk
  // Ha az aktuális az első, akkor az elészúrás ua. mint az első helyre beszúrás
  if (isFirst()) {
    insertFirst(e); // Tehát szúrjuk simán az elejére
  } else {
    // Ha nem az cur az első,
    // Létrehozzuk az új elemet, és egyben beállítjuk a mutatóit az cur-ra, és
    // az cur megelőzőjére
    Node *p = new Node(e, cur->prev, cur);
    cur->prev->next = p; // cur megelőzőjének rákövetkezője innentől az új elem
    cur->prev = p; // Az cur megelőzője is az új elem
    cur = p; // Mivel az átláncolás megvan, innetől az új elem az aktuális
  }
}

// Az aktuális elem után szúrja be a paraméterben kapott új értéket
template <class T> void List<T>::insertAfter(T e) {
  if (!cur)                    // Ha nincs aktuális elem
    throw(CurNullException()); // Akkor CurNullException-t dobunk
  // Ha az aktuális az utolsó, akkor az utána szúrás ua. mint az utolsó helyre
  // szúrás
  if (isLast()) {
    insertLast(e); // Tehát szúrjuk simán a végére
  } else {
    // Ha nem az cur az utolsó,
    // Létrehozzuk az új elemet, és beállítjuk a
    // mutatóit az cur-ra, és az cur rákövetkezőjére
    Node *p = new Node(e, cur, cur->next);
    // Az cur rákövetkezőjének megelőzője innentől az új elem
    cur->next->prev = p;
    cur->next = p; // Az cur rákövetkezője is az új elem
    cur = p; // Mivel az átláncolás megvan, innentől az új elem az aktuális
  }
}

/// Törlő metódusok implementálása
// Törli a lista első elemét
template <class T> void List<T>::removeFirst() {
  if (isEmpty())                 // Ha üres a lista, akkor nem lehet törölni
    throw(UnderFlowException()); // Tehát dobunk egy UnderFlowException-t
  if (head == tail)              // Ha csak 1 darab elem van a listában
  {
    delete head; // Kitöröljük azt az egyet
    cur = head = tail =
        nullptr; // Mivel a lista így kiürült, a mutatókat lenullázzuk
  } else         // Ha több elem van
  {
    head = head->next;    // A head eggyel hátrébb kerül
    delete head->prev;    // Az első elemet töröljük
    head->prev = nullptr; // Majd a rá mutató mutatót lenullázzuk
    cur = head;           // Az első lesz az aktuális elem
  }
}

// Törli a lista utolsó elemét
template <class T> void List<T>::removeLast() {
  if (isEmpty())                 // Ha üres a lista, akkor nem lehet törölni
    throw(UnderFlowException()); // Tehát dobunk egy UnderFlowException-t
  if (head == tail)              // Ha csak 1 darab elem van a listában
  {
    delete tail; // Kitöröljük azt az egyet
    cur = head = tail =
        nullptr; // Mivel a lista így kiürült, a mutatókat lenullázzuk
  } else         // Ha van több elem
  {
    tail = tail->prev;    // A tail eggyel előrébb kerül
    delete tail->next;    // Az utolsó elemet töröljük
    tail->next = nullptr; // Majd a rá mutató mutatót lenullázzuk
    cur = tail;           // Az utolsó lesz az aktuális
  }
}

// Törli a lista aktuális elemét
template <class T> void List<T>::removeCur() {
  if (isEmpty())                 // Ha üres a lista, nincs mit törölni
    throw(UnderFlowException()); // Dobunk egy UnderFlowException-t
  if (!cur)                      // Ha nem üres, de az cur nullptr
    throw(CurNullException());   // Dobunk egy CurNullException-t
                                 // Ha az cur az első
  if (isFirst()) {
    removeFirst(); // Akkor simán töröljük az elsőt
  } else if (isLast()) {
    // Ha az cur az utolsó
    removeLast(); // Akkor simán töröljük az utolsót
  } else {
    // Ha köztes elem
    Node *p = cur; // Egy ideiglenes pointerrel rámutatunk az cur-ra
    cur = cur->prev; // Az cur-ot eggyel előrébb toljuk (lehetne hátra is, de mi
                     // most így valósítjuk meg a törlést)
    p->next->prev = p->prev; // A p(régi cur) rákövetkezőjének megelőzője a
                             // törlendő elem megelőzője lesz lesz
    p->prev->next = p->next; // és a p megelőzőjének rákövetkezője a törlendő
                             // elem rákövetkezője lesz
    delete p; // Ezek után kitöröljük a törlendő elemet
  }
}

/// Kiegészítő eljárás implementálása
// Kiírja konzolra a lista tartalmát az elejérõl a végéig haladva
template <class T> void List<T>::print() const {
  // Egy i pointer fut végig az elemeken, amíg a tail után ki nem fut a listából
  for (Node *i = head; i != nullptr; i = i->next) {
    std::cout << i->data << ", "; // és kiírja az aktuális elem értékét
  }
  std::cout << std::endl; // !Ez a sor már nem tartozik a for ciklushoz!
}

template <class T> typename List<T>::Iterator List<T>::begin() const {
  return Iterator(head);
}

template <class T> typename List<T>::Iterator List<T>::end() const {
  return Iterator(nullptr);
}

template <class T> typename List<T>::Iterator List<T>::last() const {
  return Iterator(tail);
}

template <class T> typename List<T>::Iterator List<T>::find(const T &e) const {
  Node *p;
  for (p = head; p != nullptr && !(p->data == e); p = p->next)
    cur = p; // Az aktuális mutatót a megtalált elemre állítjuk
  return Iterator(p);
}

template <typename T>
std::ostream &operator<<(std::ostream &o, const List<T> &list) {
  for (typename List<T>::Iterator it = list.begin(); it != list.end(); ++it) {
    o << *it << " ";
  }
  return o;
}

#endif // LIST_HPP_INCLUDED

