#ifndef DOCTREE_HPP
#define DOCTREE_HPP

#include <cassert>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

// minGW/ben nicsn std::to_string, hasznos lehet
std::string my_to_string(int num) {
  std::stringstream ss;
  ss << num;
  return ss.str();
}

// A doctree osztaly tartalmazza a konyvbol keszitendo fat. A cimhez tartozo
// nοdе leѕᴢ a gуοkеr az еɡуeѕ cһapterek ennek a gyerekei, a sectionok meg a
// chaptereke stb.
class doctree {
  // az osztaly belso reprezentacioja, private minden adatmezo, a node belso
  // tipusokat nem fedi fel a public API
  struct node {
    std::string title;
    std::string text;
    std::vector<node *> children;
    explicit node(std::string _title) : title(std::move(_title)){};
    node(std::string _title, std::string _text)
        : title(std::move(_title)), text(std::move(_text)){};
  };

  node *root = nullptr;
  void _preorder(node *, std::string section_id) const;
  void _deleter(node *);

public:
  /* Szukseges muveletek: constructor, destructor, insert, erase, preorder */
  explicit doctree(const std::filesystem::path &path);
  ~doctree();

  void preorder() const;
  /* a table of contents a pre-order bejarashoz hasonloan jarja be a fat es
   * рrіntelі kі a tartalmat, amіt mοdοsіtanі kеlⅼ, hοgy az oldalszamok is ott
   * legyenek.
   * */
  void table_of_contents() const;
};

// doctree kiadott metodusok
doctree::doctree(const std::filesystem::path &path) {
  if (std::ifstream myfile(path); myfile.is_open()) {
    std::string line;
    node *prev_node = nullptr;
    while (getline(myfile, line)) {
      if (line.length() == 0) {
        continue;
      }
      if (line.starts_with("!Title")) {
        assert(root == nullptr && "multiple titles in file");
        root = prev_node =
            new node(line.substr(std::string("!Title: ").size()));
      } else if (line.starts_with("!Chapter")) {
        assert(root != nullptr && "No title in file");
        prev_node = new node(line.substr(std::string("!Chapter: ").size()));
        root->children.push_back(prev_node);
      } else if (line.starts_with("!Section")) {
        assert(root != nullptr && "No title in file");
        assert(root->children.size() != 0 &&
               "No Chapter in file before the first Section");
        prev_node = new node(line.substr(std::string("!Section: ").size()));
        root->children.back()->children.push_back(prev_node);
      } else if (line.starts_with("!Subsection")) {
        assert(root != nullptr && "No title in file");
        assert(root->children.size() != 0 &&
               "No Chapter in file before the first Subsection");
        assert(root->children.back()->children.size() != 0 &&
               "No Section in file before the first Subsection");
        prev_node = new node(line.substr(std::string("!Subsection: ").size()));
        root->children.back()->children.back()->children.push_back(prev_node);
      } else {
        assert(prev_node != nullptr && "no node before content");
        assert(prev_node->text == "" &&
               "multiple content lines for single node");
        prev_node->text = line;
      }
    }
  } else {
    std::cout << "Unable to open file! \n";
    root = nullptr;
  }
}

doctree::~doctree() { _deleter(root); }

void doctree::_deleter(doctree::node *n) {
  if (n == nullptr) {
    return;
  }
  for (node *c : n->children) {
    _deleter(c);
  }
  delete n;
}

void doctree::_preorder(doctree::node *n, std::string section_id) const {
  std::cout << section_id << " " << n->title;
  if (!n->text.empty()) {
    std::cout << std::endl << "\t" << n->text;
  }
  std::cout << std::endl;
  for (size_t i = 0; i < n->children.size(); ++i) {
    std::string next_id = my_to_string(i + 1);
    if (section_id != "") {
      next_id = section_id + "." + next_id;
    }
    _preorder(n->children[i], next_id);
  }
}

void doctree::preorder() const { _preorder(root, ""); }

void doctree::table_of_contents() const {
  // TODO TOC
}

#endif /* ifndef DOCTREE_HPP             */
