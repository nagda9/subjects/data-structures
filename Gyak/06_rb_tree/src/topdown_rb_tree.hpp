//
// Created by bgd54 on 07/10/2020.
//

#ifndef RB_TREE_SOLUTION_TOPDOWN_RB_TREE_HPP
#define RB_TREE_SOLUTION_TOPDOWN_RB_TREE_HPP

#include "exceptions.hpp"
#include <cassert>

// Megoldott kod - statikus _min, _max, _prev, _next fuggvenyekkel

//
// Piros-fekete fa osztály
// DEFINÍCIÓ
//
template <class T> class td_rb_tree {

  // Szín felsoroló típus
  enum color_t { black, red };

  // Belső csúcs struktúra
  struct node {
    node *parent;
    node *left, *right;
    color_t color;
    T key;

    // Az alapértelmezett konstruktor: csak az empty_leaf létrehozására szolgál
    node() : parent(this), left(this), right(this), color(black), key() {}

    // Konstruktor csúcs létrehozására beszúráskor
    node(const T &k, node *p)
        : parent(p), left(empty_leaf), right(empty_leaf), color(red), key(k) {}
  };

  // Adattagok
  static inline node _empty_leaf; // node() default konstruktor meghívása ->
                                  // _empty_leaf létrejön
  static inline node *empty_leaf =
      &_empty_leaf; // empty_leaf a _empty_leaf-re mutató pointer

  node *root;

  // Felszabadító függvény
  static void _destroy(node *x);

  // Segédfüggvények
  static node *_min(node *x);
  static node *_max(node *x);
  static node *_next(node *x);
  static node *_prev(node *x);

  static size_t _size(node *x);

  // Kiegyensúlyozásért felelős függvények
  void _rotate_left(node *x);
  void _rotate_right(node *x);

  // Ellenőrző segédfüggvények
  static size_t _validate(node *x);

public:
  // Konstruktor és destruktor
  td_rb_tree() : root(empty_leaf) {}
  ~td_rb_tree() { _destroy(root); }

  // Másoló konstruktor és operátor egyelőre nincs implementálva
  td_rb_tree(const td_rb_tree &t) = delete;
  td_rb_tree &operator=(const td_rb_tree &t) = delete;

  // Alapműveletek
  [[nodiscard]] size_t size() const { return _size(root); }

  bool find(const T &k) const;
  void insert(const T &k);
  void remove(const T &k);

  // Ellenőrző függvény
  void validate() const;
};

//
// Piros-fekete fa osztály
// FÜGGVÉNYIMPLEMENTÁCIÓK
//

// Rekurzívan felszabadítja a csúcsokat.
// A destruktor hívja meg a gyökérre.
template <class T> void td_rb_tree<T>::_destroy(node *x) {
  if (x != empty_leaf) {
    _destroy(x->left);
    _destroy(x->right);
    delete x;
  }
}

// Visszaadja az x gyökerű részfa legkisebb értékű csúcsát.
// Előfeltétel: x != empty_leaf
template <class T> typename td_rb_tree<T>::node *td_rb_tree<T>::_min(node *x) {
  while (x->left != empty_leaf)
    x = x->left;
  return x;
}

// Visszaadja az x gyökerű részfa legnagyobb értékű csúcsát.
// Előfeltétel: x != empty_leaf
template <class T> typename td_rb_tree<T>::node *td_rb_tree<T>::_max(node *x) {
  while (x->right != empty_leaf)
    x = x->right;
  return x;
}

// Visszaadja a fából az x csúcs rákövetkezőjét,
// vagy empty_leaf-t, ha x a legnagyobb kulcsú elem.
// Előfeltétel: x != empty_leaf
template <class T> typename td_rb_tree<T>::node *td_rb_tree<T>::_next(node *x) {
  if (x->right != empty_leaf)
    return _min(x->right);

  node *y = x->parent;
  while (y != empty_leaf && x == y->right) {
    x = y;
    y = y->parent;
  }
  return y;
}

// Visszaadja a fából az x csúcs megelőzőjét,
// vagy empty_leaf-t, ha x a legkisebb kulcsú elem.
// Előfeltétel: x != empty_leaf
template <class T> typename td_rb_tree<T>::node *td_rb_tree<T>::_prev(node *x) {
  if (x->left != empty_leaf)
    return _max(x->left);

  node *y = x->parent;
  while (y != empty_leaf && x == y->left) {
    x = y;
    y = y->parent;
  }
  return y;
}

// Rekurzívan meghatározza, és visszaadja
// az x gyökerű részfa elemeinek számát.
// Megjegyzés: üres fára is működik -> 0-t ad vissza
template <class T> size_t td_rb_tree<T>::_size(node *x) {
  if (x == empty_leaf)
    return 0;
  else
    return _size(x->left) + _size(x->right) + 1;
}

// Balra forgatás ...
// az x csúcs körül, illetve más szóhasználattal
// az x csúcs és a jobb gyereke közötti él mentén.
template <class T> void td_rb_tree<T>::_rotate_left(node *x) {
  // y-nak nevezzük el x jobb gyerekét
  // a forgatás az x-y él mentén történik
  node *y = x->right;

  // y-nak mindenképpen létező csúcsnak kell lennie
  if (y == empty_leaf)
    throw internal_error();

  // y bal gyereke forgatás után x jobb gyereke lesz
  // a gyerek szülő mezőjét is frissíteni kell
  x->right = y->left;
  if (y->left != empty_leaf) /* a feltétel opcionális */
    y->left->parent = x;

  // az adott részfának mostantól y lesz a gyökere
  // így megkapja x szülőjét, és
  // a szülőnél is be kell állítani, hogy mostantól y az ő gyereke
  y->parent = x->parent;
  if (x->parent == empty_leaf)
    root = y;
  else if (x == x->parent->left)
    x->parent->left = y;
  else
    x->parent->right = y;

  // végül beállítjuk x és y között a szülő-gyerek kapcsolatot
  y->left = x;
  x->parent = y;
}

// Jobbra forgatás ...
// az x csúcs körül, illetve más szóhasználattal
// az x csúcs és a bal gyereke közötti él mentén.
template <class T> void td_rb_tree<T>::_rotate_right(node *x) {
  // y-nak nevezzük el x bal gyerekét
  // a forgatás az x-y él mentén történik
  node *y = x->left;

  // y-nak mindenképpen létező csúcsnak kell lennie
  if (y == empty_leaf)
    throw internal_error();

  // y jobb gyereke forgatás után x bal gyereke lesz
  // a gyerek szülő mezőjét is frissíteni kell
  x->left = y->right;
  if (y->right != empty_leaf) /* a feltétel opcionális */
    y->right->parent = x;

  // az adott részfának mostantól y lesz a gyökere
  // így megkapja x szülőjét, és
  // a szülőnél is be kell állítani, hogy mostantól y az ő gyereke
  y->parent = x->parent;
  if (x->parent == empty_leaf)
    root = y;
  else if (x == x->parent->left)
    x->parent->left = y;
  else
    x->parent->right = y;

  // végül beállítjuk x és y között a szülő-gyerek kapcsolatot
  y->right = x;
  x->parent = y;
}

// Lekérdezi, hogy található-e k kulcs a fában.
// Igazat ad vissza, ha található.
template <class T> bool td_rb_tree<T>::find(const T &k) const {
  node *x = root;
  while (x != empty_leaf && k != x->key)
    if (k < x->key)
      x = x->left;
    else
      x = x->right;
  return x != empty_leaf;
}

// Beszúrja a k értéket a fába.
// Ha már van k érték a fában, akkor nem csinál semmit.
// Lefele menet atszinez es forgat, hogy a beszurt csucs szuleje biztos fekete
// legyen
// Heurisztika: felfele viszem a pirosat
template <class T> void td_rb_tree<T>::insert(const T &k) {
  // Gyoker kezeles
  if (root == empty_leaf) {
    root = new node(k, empty_leaf);
    root->color = black;
    return;
  }

  node *g = nullptr;    // grandparent
  node *p = empty_leaf; // parent
  node *x = root;       // current node
  while (true) {
    if (empty_leaf == x) {
      x = new node(k, p);
      if (k > p->key) {
        p->right = x;
      } else {
        p->left = x;
      }
    } else if (x->left->color == red && x->right->color == red) {
      //            x:black   ->        x:red
      //           / \                 / \
      //       l:red   r:red     l:black   r:black
      //  Mindket gyerek piros -> x feketejet letolom, fekete magassag nem
      //  valtozik, lefele szabaly nem serul. Felfele lehet gond ha p piros
      //  (kovetkezo blokk javitja)
      //           [x]   ->   (x)
      //           / \   ->   / \
      //         (l) (r) -> [x] [r]
      //
      x->color = red;
      x->right->color = black;
      x->left->color = black;
    }
    assert((x != root || p->color != red) &&
           "root with red parent during insert");
    /* Fix red violation */
    if (x->color == red && p->color == red) {
      assert(g && "grandparent is nullptr, x is root and p is red which is an "
                  "invalid state");
      if (g->left == p) {
        if (x == p->right) { // x ellentetes oldali gyerek mint a szulo -> dupla
                             // forgatas
          _rotate_left(p);
          // x azonos oldali gyerek
          assert(g->right->color == black && "rebalance leads to double red");
          _rotate_right(g);
          g->color = red;
          x->color = black;
        } else {
          // x azonos oldali gyerek
          assert(g->right->color == black && "rebalance leads to double red");
          _rotate_right(g);
          g->color = red;
          p->color = black;
        }
      } else {
        if (x == p->left) { // x ellentetes oldali gyerek mint a szulo -> dupla
                            // forgatas
          _rotate_right(p);
          assert(g->left->color == black && "rebalance leads to double red");
          _rotate_left(g);
          g->color = red;
          x->color = black;
        } else {
          // x azonos oldali gyerek
          assert(g->left->color == black && "rebalance leads to double red");
          _rotate_left(g);
          g->color = red;
          p->color = black;
        }
      }
    }

    if (x->key == k) {
      break;
    }
    // step to next level
    g = std::exchange(p, x);
    if (k < x->key)
      x = x->left;
    else
      x = x->right;
  }
  root->color = black;
  empty_leaf->parent = empty_leaf;
}

// Eltávolítja a k értéket a fából.
// Ha nem volt k érték a fában, akkor nem csinál semmit.
// otlet: piros torles okes->letolok pirosat h tuti azt toroljek
template <class T> void td_rb_tree<T>::remove(const T &k) {
  node *p = empty_leaf;
  node *x = root;
  node *found = nullptr;
  if (root == empty_leaf)
    return;
  if (x->left->color == black && x->right->color == black) {
    // Ha mindket gyereke fekete akkor lelepunk egyet piros a gyoker es lelepunk
    // egyet
    x->color = red;
    p = x;
    if (x->key == k) {
      found = x;
    }
    if (k < x->key)
      x = x->left;
    else
      x = x->right;
  } // ha nem akkor x marad a gyoker

  // ciklus alapfeltevese: p piros x a vizsgalt node mivel p piros s fekete,
  // x-et pirosra festjuk ezert p-vel utkozni fog, javítani kell
  while (x != empty_leaf) {
    /* Save found node */
    if (x->key == k) {
      found = x;
    }
    // x black p red, s black
    // fessuk xet pirosra, esetek x es s gyerekei szerint
    if (p->left == x) {
      if (x->color == black) {
        node *s = p->right;
        if (x->left->color == black && x->right->color == black) {
          if (s->left->color == black && s->right->color == black) {
            // mind a 4 gyerek fekete
            assert(s->color == black && (p->color == red || p == empty_leaf));
            s->color = red; // this might color empty_leaf red
            x->color = red;
            p->color = black;
            // move down 1 level
          } else { // s legalabb 1 gyereke prios
            if (s->right->color != red) {
              // ha nem a bal a.k.a. ha azonos oldali mint x p-nek
              _rotate_right(s);
              s->color = red;
              s->parent->color = black;
              s = s->parent;
            }
            // s jobb gyereke piros, s fekete
            _rotate_left(p);
            p->color = black;
            s->right->color = black;
            s->color = red;
            x->color = red;
          }
        } else {
          if (k < x->key &&
              x->right->color == red) { // balra lepnenk jobbra piros
            _rotate_left(x);
            x->parent->color = black;
            x->color = red;
          } else if (k >= x->key &&
                     x->left->color == red) { // jobbra lepnenk balra piros
            _rotate_right(x);
            x->parent->color = black;
            x->color = red;
          }
        }
      }
    } else { // p jobb gyereke x
      if (x->color == black) {
        node *s = p->left;
        if (x->left->color == black && x->right->color == black) {
          if (s->left->color == black && s->right->color == black) {
            // mind a 4 gyerek fekete
            assert(s->color == black && (p->color == red || p == empty_leaf));
            s->color = red; // this might color empty_leaf red
            x->color = red;
            p->color = black;
            // move down 1 level
          } else { // s legalabb 1 gyereke prios
            if (s->left->color != red) {
              // ha nem a jobb a.k.a. ha azonos oldali mint x p-nek
              _rotate_left(s);
              s->color = red;
              s->parent->color = black;
              s = s->parent;
            }
            // s bal gyereke piros, s fekete
            _rotate_right(p);
            p->color = black;
            s->left->color = black;
            s->color = red;
            x->color = red;
          }
        } else {
          if (k < x->key &&
              x->right->color == red) { // balra lepnenk jobbra piros
            _rotate_left(x);
            x->parent->color = black;
            x->color = red;
          } else if (k >= x->key &&
                     x->left->color == red) { // jobbra lepnenk balra piros
            _rotate_right(x);
            x->parent->color = black;
            x->color = red;
          }
        }
      }
    }

    // step to next level
    p = x;
    if (k < x->key)
      x = x->left;
    else
      x = x->right;
  }

  if (nullptr != found) {
    node *c;
    if (p->left != empty_leaf)
      c = p->left;
    else
      c = p->right;
    // FIGYELEM: A következő sor módosíthatja a empty_leaf csúcsot!
    c->parent = p->parent;
    if (p->parent == empty_leaf)
      root = c;
    else if (p == p->parent->left)
      p->parent->left = c;
    else
      p->parent->right = c;

    if (p != found)
      found->key = p->key;
    delete p;
  }
  empty_leaf->parent = empty_leaf;
  root->color = black;
  assert(root->color == black && root->parent == empty_leaf);
  assert(empty_leaf->color == black && empty_leaf->parent == empty_leaf &&
         empty_leaf->right == empty_leaf && empty_leaf->left == empty_leaf);
}


// Rekurzív segédfüggvény a piros-fekete tulajdonságok ellenőrzéséhez
// Paraméterül kapja az ellenőrizendő részfa gyökerét, és visszaadja
// a részfa fekete-magasságát.
template <class T> size_t td_rb_tree<T>::_validate(node *x) {
  // empty_leaf fekete-magassága nulla
  if (x == empty_leaf)
    return 0;

  // "Minden csúcs színe piros vagy fekete."
  if (x->color != red && x->color != black)
    throw invalid_rb_tree();

  // "Minden piros csúcs mindkét gyereke fekete."
  if (x->color == red && x->parent->color == red)
    throw invalid_rb_tree();

  // Rekurzív ellenőrzés és fekete-magasság meghatározása
  size_t left_black_height = _validate(x->left);
  size_t right_black_height = _validate(x->right);

  // "Bármely gyökértől levélig vezető úton
  // a fekete csúcsok száma egyenlő."
  if (left_black_height != right_black_height)
    throw invalid_rb_tree();

  // Visszaadjuk a részfa fekete-magasságát
  return left_black_height + (x->color == black);
}

// Ez a függvény a debugolást segíti.
// Ellenőrzi, hogy a gyökérből elérhető fa érvényes
// bináris keresőfa, illetve érvényes piros-fekete fa-e.
template <class T> void td_rb_tree<T>::validate() const {
  // Keresőfa tulajdonság ellenőrzése bejárással
  if (root != empty_leaf) {
    node *x = _min(root);
    T prev = x->key;
    while ((x = _next(x)) != empty_leaf) {
      if (prev >= x->key)
        throw invalid_binary_search_tree();
      prev = x->key;
    }
  }

  //
  // Piros-fekete fa tulajdonságok ellenőrzése
  //

  // empty_leaf csúcs ellenőzése
  // "Minden levél színe fekete."
  if (empty_leaf->left != empty_leaf || empty_leaf->right != empty_leaf ||
      empty_leaf->color != black)
    throw invalid_rb_tree();

  // "A gyökér színe fekete."
  if (root->color != black)
    throw invalid_rb_tree();

  // A fa rekurzív ellenőrzése
  _validate(root);
}

#endif // RB_TREE_SOLUTION_TOPDOWN_RB_TREE_HPP
