#ifndef DYNAMICSTACK_H_
#define DYNAMICSTACK_H_

#include <iostream>

#include "../exceptions/listexception.hpp"

template <typename T> class DynamicStack {
public:
  DynamicStack() : head{nullptr} {}
  ~DynamicStack();
  DynamicStack(const DynamicStack &other) = delete;
  DynamicStack(DynamicStack &&other) = delete;
  DynamicStack &operator=(DynamicStack rhs) = delete;
  DynamicStack &operator=(DynamicStack &&rhs) = delete;
  void push(T new_item);
  T pop();
  T top() const;
  bool isEmpty() const;
  void print() const;

private:
  struct Node {
    T value;
    Node *next;

    Node() : value{}, next(nullptr) {}
    Node(T _value) : value(_value), next(nullptr) {}
    Node(T _value, Node *_pNext) : value(_value), next(_pNext) {}
  };

  Node *head;
};

template <typename T> DynamicStack<T>::~DynamicStack() {
  while (!isEmpty()) {
    pop();
  }
}

template <typename T> bool DynamicStack<T>::isEmpty() const {
  return (head == nullptr);
}

template <typename T> void DynamicStack<T>::push(T new_item) {
  Node *p = new Node(new_item);
  p->next = head;
  head = p;
}

template <typename T> T DynamicStack<T>::top() const {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  return head->value;
}

template <typename T> T DynamicStack<T>::pop() {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  T tmp = head->value;
  Node *p = head;
  head = head->next;
  delete p;
  return tmp;
}

template <typename T> void DynamicStack<T>::print() const {
  for (Node *i = head; i != nullptr; i = i->next) {
    std::cout << i->value << " ";
  }
}

#endif /* DYNAMICSTACK_H_ */
