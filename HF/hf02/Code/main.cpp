#include <iostream>
#include "Graph.hpp"

int main() {
    /* addVertex():
     *  - Ha még nem volt csúcs, akkor létrejön a csúcs, és hozzáadogatom a felsorolt éleket egyesével, és egyúttal létrehozok még nem létező csúcsokat is
     *  - Ha új a csúcs, akkor kell legyen létező csúcsba mutató éle
     *  - Ha létező a csúcs, akkor csak olyan él szerepelhet a paraméterek között, ami már létezik a megfelelő élsúllyal (nem történik semmi), vagy teljesen új él (új csúcs is létrejön)
     * removeVertex();
     *  - Ha a csúcs létezik, akkor minden élét töröljök, kivéve, ha ezáltal független gráf keletkezik, ekkor hiba
     *  - Ha a csúcs és éleinek törlésekor egy csúcs függetlenné válik, akkor a független csúcsot is töröljük
     *  - Ha a csúcs nem létezik, akkor hiba
     * addEdge():
     *  - Ha első él, akkor mindkét csúcs létrejön az éllel együtt
     *  - Ha új él, akkor az egyik csúcsnak léteznie kell, amihez kötődik, a másik csúcs létrejön
     *  - Ha új él, és nem létezik egyik csúcs sem, akkor hiba (IndependentEdge)
     *  - Ha nem új él, és az élsúly egyezik a meglévővel, akkor nem történik semmi
     *  - Ha nem új él, és az élsúly különbözik, akkor az él felülíródik
     * removeEdge():
     *  - Ha nem létező élt akarunk törölni, ne történjen semmi
     *  - Ha olyan élt törlünk, ami után a gráf több mint egy csúcsa nem tud kapcsolódni, akkor hiba
     *  - Ha olyan élt törlünk, ami után egy csúcs független lesz, akkor azt töröljük
     * */


    try {
        { // test1 addEdge, addVertex
            Graph g;
            g.addEdge("A", "B", 4); // él beszúrása amikor nincsenek még csúcsok -> csúcsok létrehozása
            g.addEdge("A", "B", 4); // no problem
            g.addEdge("A", "B", 5); // felülírás
            g.addEdge("A", "C", 3); // no problem
            // g.addEdge("E", "F", 2); // IndependentEdge exception
            g.addVertex("D", {{"B", 2},
                              {"E", 4}});
            g.addEdge("D", "A", 1);
            g.addVertex("A", {{"B", 6},
                              {"F", 9}});

            // g.exportGraph("proba");
            g.print();
        }

        { // test2 read file
            Graph g;
            g.readGraph("../../input/graph_03.txt");
            // g.exportGraph("proba");
            g.print();
            g.printDijsktraSP("0");
        }

        { // test3 DSP
            Graph g;
            g.addEdge("A", "B", 6);
            g.addEdge("A", "D", 1);
            g.addEdge("D", "B", 2);
            g.addEdge("D", "E", 1);
            g.addEdge("B", "E", 2);
            g.addEdge("B", "C", 5);
            g.addEdge("E", "C", 5);
            g.print();
            // g.exportGraph("proba");
            g.printDijsktraSP("A");
        }

        { // test4 PrimMST
            Graph g;
            g.readGraph("../../input/graph_01.txt");
            // g.exportGraph("proba");
            Graph h = g.PrimMST();
            // h.exportGraph("tree");
            g.printPrimMST();
        }

        { // test5 removeEdge
            Graph g;
            g.readGraph("../../input/graph_05.txt");
            g.print();
            g.removeEdge("x", "y");
            g.removeEdge("e","f");
            g.print();
            g.removeEdge("e", "c");
            g.print();
            g.removeEdge("b", "d");
            g.print();
        }

        { // test6 removeVertex
            Graph g;
            g.readGraph("../../input/graph_05.txt");
            g.print();
            g.removeVertex("x");
            g.print();
            // g.removeVertex("c");
            g.removeVertex("e");
            g.print();
        }

    }
    catch (exception& e){
        cerr << e.what() << endl;
    }
}
