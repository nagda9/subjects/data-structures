cmake_minimum_required(VERSION 3.13)
project(hf03 CXX)

#set(CMAKE_CXX_STANDARD 20)
#set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(woodpecker)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic")

#option(USE_WERROR "turn on -Werror flag" ON)
#if(USE_WERROR)
#    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror")
#endif(USE_WERROR)

add_executable(hf03 tests_main.cpp)
target_link_libraries(hf03 PRIVATE woodpecker)