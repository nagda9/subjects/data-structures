//
// Created by Balazs Nagy on 2020. 09. 12..
//

#include "BigBrother.h"

BigBrother::BigBrother() {
    for(size_t i = 0; i < 5; ++i) {
        stations.push_back(BaseStation(names[i], Position2D(rand() % 100, rand() % 100)));
    }

    subjects.push_back(new Mobile("iPhone", Position2D(rand() % 100, rand() % 100)));
    subjects.push_back(new Notebook("Notebook", Position2D(rand() % 100, rand() % 100)));

    std::cout << "BaseStations are created: \n";
    for(auto s : stations)
        std::cout << s.getName() << " at coord: (" << s.getPos().x << ", " << s.getPos().y << ")\n";
    std::cout << ".........................................\n";

    for(auto s : subjects)
        std::cout << s->getName() << " start position is: (" << s->getPos().x << ", " << s->getPos().y << ")\n";

    std::cout << ".........................................\n";

    for(auto& s : stations) {
        subjects[0]->attach(&s);
        subjects[1]->attach(&s);
    }
}

BigBrother::~BigBrother() {
    for(auto& s : subjects)
        delete s;
}

void BigBrother::on_update() {
    for(auto& s : subjects)
        s->moveOtherPosition(Position2D(rand() % 100, rand() % 100));
}
