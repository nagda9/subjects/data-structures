cmake_minimum_required(VERSION 3.14)
project(fstack)

set(CMAKE_CXX_STANDARD 14)

file(GLOB SOURCES "src/*")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -Wextra -pedantic")

# hack to find exceptions lib without installing it properly
add_subdirectory(../Exceptions ../Exceptions)

add_executable(fstack ${SOURCES})
target_link_libraries(fstack example_exceptions)
