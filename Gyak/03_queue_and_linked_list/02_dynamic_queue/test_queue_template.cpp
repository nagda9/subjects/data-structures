#include "DynamicQueue.h"
#include <exception>
#include <iostream>
#include <string>

using namespace std;

int main() {
  cout << "======================" << endl;
  cout << "Test the integer queue" << endl;
  cout << "======================" << endl;

  DynamicQueue<int> int_queue;

  try {
    cout << "\n/**********In to Queue**********\\" << endl;
    for (int i = 0; i < 11; ++i) {
      int_queue.in(i);
    }

    cout << "After 11 items: ";
    int_queue.print();
    cout << endl;

    cout << "\n/**********Out from Queue**********\\" << endl;
    int first = int_queue.first();
    cout << "first(): " << first << endl;
    cout << "out():";
    for (int i = 0; i < 11; ++i) {
      int result = int_queue.out();
      cout << " " << result;
    }
    cout << endl;
  } catch (exception &e) {
    cout << e.what() << endl;
  }

  cout << "=====================" << endl;
  cout << "Test the string queue" << endl;
  cout << "=====================" << endl;

  DynamicQueue<string> char_queue;

  try {
    cout << "\n/**********In to Queue**********\\" << endl;
    char_queue.in("one");
    char_queue.in("two");
    char_queue.in("three");

    cout << "After 3 items: ";
    char_queue.print();
    cout << endl;

    cout << "\n/**********Out from Queue**********\\" << endl;
    string first = char_queue.first();
    cout << "first(): " << first << endl;
    cout << "out():";
    for (int i = 0; i < 3; ++i) {
      string result = char_queue.out();
      cout << " " << result;
    }
    cout << endl;
  } catch (exception &e) {
    cout << e.what() << endl;
  }

  return 0;
}

