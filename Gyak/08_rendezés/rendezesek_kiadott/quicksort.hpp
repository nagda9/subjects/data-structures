#ifndef QUICKSORT_HPP
#define QUICKSORT_HPP

#include <cstddef>

/// Segéd függvény
void swapInt(int &a, int &b) { // Egészeket cserélő függvény
  a = a + b;                   // Nincs szükség segédváltozóra
  b = a - b;
  a = a - b;
}

/// ========== GYORS RENDEZÉS ==========
// A gyorsrendező felosztó függvénye
int divide(int *a, int down, int up) {
  (void)a;
  (void)down;
  (void)up;
  // TODO
  return 0;
}

void quickSort(int *A, int down, int up) {
  (void)A;
  (void)down;
  (void)up;
  // TODO
}

void quickSort(int *A, int len) {
  (void)A;
  (void)len;
  // TODO
}

#endif
