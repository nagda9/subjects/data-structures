cmake_minimum_required(VERSION 3.14)
project(multi)

set(CMAKE_CXX_STANDARD 14)

file(GLOB SOURCES "src/*")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -Wextra -pedantic")

add_executable(multi ${SOURCES})