#ifndef DSTACK_SMARTDYNAMICINTSTACK_HPP
#define DSTACK_SMARTDYNAMICINTSTACK_HPP

#include <memory>

class SmartDynamicIntStack {
private:
    class Node {
    public:
        int value;
        std::unique_ptr<Node> pNext;

        Node() : value(0), pNext(nullptr) {}
        Node(const int& _value) : value(_value), pNext(nullptr) {}
        Node(const int& _value, Node* _pNext) :	value(_value), pNext(_pNext) {}
    };
    std::unique_ptr<Node> pHead;

public:
    SmartDynamicIntStack() = default;
    SmartDynamicIntStack(const SmartDynamicIntStack& other);
    SmartDynamicIntStack(SmartDynamicIntStack&& other) = default;
    SmartDynamicIntStack& operator= (const SmartDynamicIntStack& rhs);
    SmartDynamicIntStack& operator= (SmartDynamicIntStack&& rhs) = default;
    ~SmartDynamicIntStack() = default;

    void push(int new_item);
    int pop();
    int top() const;
    bool isEmpty() const;
    void print() const;
};

#endif //DSTACK_SMARTDYNAMICINTSTACK_HPP
