//
// Created by Balazs Nagy on 2020. 09. 12..
//

#include "Application.h"

#include <iostream>

namespace app {

    Application::Application(const std::string& _name) {
        app_name = _name;
    }

    void Application::attach_layer(Layer * l) {
        layers.push_back(l);
    }

    void Application::run() {
        for(unsigned int i = 0; i < 3; ++i) {

            for(auto& l : layers)
                l->on_update();

            std::cout << "Update count: " << i << "\n";
        }

    }

}