#ifndef HASHTABLE_HPP_INCLUDED
#define HASHTABLE_HPP_INCLUDED

#include <iostream>
#include <list>
#include <vector>

// HashTable osztály
// Halmazként működik - minden elemet csak egyszer tárolunk el benne
class HashTable {
  std::vector<std::list<int>> table; // A hashtábla tárterülete
  int capacity; // Hashtábla kapacitása (különböző hashek száma)
                // Nem egyenlő a maximális mérettel, tetszőleges mennyiségű
                // elemet el tudunk tárolni!
  int actualsize; // Hashtáblában éppen megtalálható elemek száma
                  // Nem egyenlő a kapacitással!
public:
  // Konstruktor, a hashtábla kapacitását kell átadni neki
  explicit HashTable(int capacity)
      : table(capacity), // Létrehozza az adott méretű vektort a hashtáblához
        capacity(capacity), actualsize(0) {}

  // Visszaadja a hashtáblában található elemek számát
  int size() { return actualsize; }

  // Visszaadja, hogy üres-e a hastábla
  bool empty() { return actualsize == 0; }

  // Kitörli az összes elemet a hashtáblából
  void clear() {
    for (int i = 0; i < capacity; ++i) {
      table[i].clear();
    }

    actualsize = 0;
  }

  // Új elemet szúr be a hashtáblába, ha nincs még ilyen elem benne
  // Ha van már ilyen elem, nem csinál semmit
  void insert(const int &key) {
    int myHash = getHash(key);
    for (std::list<int>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        // Már szerepel az elem, nem csinálunk semmit
        return;
      }
    }
    table[myHash].push_back(key);
    ++actualsize;
  }

  // Megkeresi, hogy egy adott elem megtalálható-e a hashtáblában
  bool find(const int &key) {
    int myHash = getHash(key);
    for (std::list<int>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        return true;
      }
    }
    return false;
  }

  // Kitöröl egy adott elemet, ha megtalálható a hashtáblában
  // Ha nem található meg, nem csinál semmit
  void erase(const int &key) {
    int myHash = getHash(key);
    for (std::list<int>::iterator it = table[myHash].begin();
         it != table[myHash].end(); ++it) {
      if (*it == key) {
        table[myHash].erase(it);
        --actualsize;
        return;
      }
    }
  }

  // Kiírja a hashtábla tartalmát
  // Soronként egy hashérték, majd szóközzel elválasztva a benne található
  // értékek
  void print() {
    for (int i = 0; i < capacity; ++i) {
      std::cout << i << ": ";
      for (std::list<int>::iterator it = table[i].begin(); it != table[i].end();
           ++it) {
        std::cout << *it << " ";
      }
      std::cout << std::endl;
    }
  }

private:
  // Visszaadja egy adott kulcs hashét
  int getHash(const int &key) {
    // Egyszerű hashfüggvény: csak visszaadja a számot modulózva a kapacitással
    return key % capacity;
  }
};

#endif // HASHTABLE_HPP_INCLUDED
