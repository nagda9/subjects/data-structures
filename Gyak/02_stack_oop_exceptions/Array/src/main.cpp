#include <iostream>

#include "FixedArray.hpp"
#include "DynamicArray.hpp"

using namespace std;

void try_array(AbstractArray* a) {
	for (int i = 0; i < 20; i++) {
		a->push_back(i * 10);
	}

	for (int i = 0; i < a->size(); i++) {
		cout << (*a)[i] << " ";
	}
	cout << endl;
}

int main() {
	cout << "Arrays" << endl;

	FixedArray* fixed_array = new FixedArray(20);
	DynamicArray* dynamic_array = new DynamicArray(2);

	try_array(fixed_array);
	try_array(dynamic_array);

	delete fixed_array;
	delete dynamic_array;

	return 0;
}
