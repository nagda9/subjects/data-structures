#include "Subject.h"

Subject::Subject()
{

}

Subject::~Subject()
{

}

Position2D Subject::getPos() {
    return pos;
}

void Subject::notify(Position2D _pos) {
    for(auto it = stations.begin(); it != stations.end(); ++it) {
        if(*it)
            (*it)->update(_pos);
    }
}

void Subject::attach(BaseStation* s) {
    stations.push_back(s);
}

void Subject::detach(BaseStation* s) {
    stations.erase(std::remove(stations.begin(), stations.end(), s), stations.end());
}