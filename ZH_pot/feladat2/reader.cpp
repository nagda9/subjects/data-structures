#include "doctree.hpp" // <a megoldasod a doctree.hpp-be irhatod            
#include <fstream>
#include <iostream>
#include <string>
using namespace std;

// írj a működést bemutató main-t, és teszteld magadnak :)
int main() {
  doctree dt("text_shmall.txt");
  dt.preorder();
  cout << endl;
  dt.table_of_contents();
  return 0;
}
