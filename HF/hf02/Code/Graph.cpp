#include "Graph.hpp"

void Graph::addEdge(const string& from, const string& to, int val) {
    if (vertices.empty()) { // elsó él
        Node* new_from = new Node(from);
        Node* new_to = new Node(to);

        Edge* new_edge = new Edge(from, to, val);
        edges[*new_edge] = new_edge;

        new_from->edges[to] = new_edge;
        new_to->edges[from] = new_edge;

        vertices[from] = new_from;
        vertices[to] = new_to;
    } else {
        if (vertices.contains(from) && vertices.contains(to)) { // már mindkét csúcs létezik
            if (edges.contains(Edge(from, to, val))){
                return;
            }
            else { // nem tartalmazza, és nem is ugyanaz más értékkel
                Edge* new_edge = new Edge(from, to, val);
                edges[*new_edge] = new_edge;

                if (vertices[from]->edges.contains(to)) { // már van from és to között él, csak a val tér el, pl.: A-B 4, A-B 2
                    // A és B élét nullptr-ezem, felszabadítom A-B 4-et, majd átadom A-B 2-t
                    Edge* old_edge(vertices[from]->edges[to]); // az élre ráállítok egy új átmeneti pointert
                    vertices[from]->edges.erase(to); // kitörlöm az élek memóriaterületét a csúcsokhoz rendelt élek közül
                    vertices[to]->edges.erase(from);
                    edges.erase(*old_edge); // kitörlöm az élt az élek közül
                    delete old_edge; // felszabadítom az élt
                    //old_edge = nullptr; // biztonságból nullptr-re állítom, nehogy használjam
                }

                // új él létrehozása
                // pl.: A-B 4, D-B 2; és A-B 4, A-B 2 esetben is
                vertices[from]->edges[to] = new_edge;
                vertices[to]->edges[from] = new_edge;
            }
        }
        else if (vertices.contains(from)) { // csak a from csúcs létezik
            Edge* new_edge = new Edge(from, to, val);
            edges[*new_edge] = new_edge;

            vertices[from]->edges[to] = new_edge;

            Node* new_to = new Node(to);
            new_to->edges[from] = new_edge;
            vertices[to] = new_to;
        }
        else if (vertices.contains(to)) { // csak a to csúcs létezik
            Edge* new_edge = new Edge(from, to, val);
            edges[*new_edge] = new_edge;

            vertices[to]->edges[from] = new_edge;

            Node* new_from = new Node(from);
            new_from->edges[to] = new_edge;
            vertices[from] = new_from;
        }
        else {
            throw IndependentEdge(from, to, val);
        }
    }
}

[[maybe_unused]] void Graph::exportGraph(const string& filename) {
    ofstream kf;
    filesystem::path path = "../../output/";
    if (!filesystem::exists(path))
    {
        filesystem::create_directories(path);
    }

    kf.open(path.string() + filename + ".dot");
    kf << "strict graph {" << endl;
    stringstream ss;
    for(const auto& e : edges) {
        ss << e.first.vertex1 << " " << e.first.vertex2 << " " << e.first.val;
        string s_vertex1, s_vertex2, s_val;
        ss >> s_vertex1 >> s_vertex2 >> s_val;
        ss.clear();
        kf << "  " << s_vertex1 << " -- " << s_vertex2 << "[label=\"" << s_val << "\"];" << endl;
    }
    kf << "}" << endl;
    kf.close();
}

Graph::~Graph() {
    for (const auto& e : edges){
        delete e.second;
    }
    for (const auto& v : vertices) {
        delete v.second;
    }
}

[[maybe_unused]] void Graph::addVertex(const string& vertex, const vector<pair<string, int>>& edge_vector) {
    if (vertices.empty()) { // még üres a gráf
        Node* new_from = new Node(vertex);
        vertices[vertex] = new_from;
        for (const auto& e : edge_vector) {
            addEdge(vertex, e.first, e.second);
        }
    }
    else {
        if (!vertices.contains(vertex)){ // új csúcs
            bool isConnecting = false;
            for (const auto& e : edge_vector) {
                if (vertices.contains(e.first)) {
                    isConnecting = true;
                    break;
                }
            }
            if (isConnecting) {
                Node* new_from = new Node(vertex);
                vertices[vertex] = new_from;
                for (const auto& e : edge_vector) {
                    addEdge(vertex, e.first, e.second);
                }
            }
            else {
                throw IndependentVertex(vertex);
            }
        }
        else { // meglévő csúcs
            for (const auto& e : edge_vector) {
                addEdge(vertex, e.first, e.second);
            }
        }

    }
}

[[maybe_unused]] void Graph::print() {
    for (const auto& v : vertices) {
        cout << v.first << " ";
    }
    cout << endl;

    for (const auto& e : edges) {
        cout << e.first.vertex1 << "--" << e.first.vertex2 <<  "[ " << e.first.val << " ]" << endl;
    }
    cout << endl;
}

[[maybe_unused]] void Graph::readGraph(const string &filename) {
    ifstream bf;
    bf.open(filename);
    if (!bf.good()) {
        cerr << "Could not open file!" << endl;
    }

    string from;
    string line_tos;
    string line_vals;
    while(!bf.eof()) {
        bf >> from >> ws;
        getline(bf, line_tos);
        getline(bf, line_vals);

        vector<string> tos;
        vector<int> vals;

        string to;
        for (size_t i = 0; i < line_tos.size(); i++) {
            if (line_tos[i] == ','){
                tos.push_back(to);
                to = "";
            }
            else if (i == line_tos.size()-1) {
                to += line_tos[i];
                tos.push_back(to);
            }
            else {
                to += line_tos[i];
            }
        }

        int val;
        stringstream ss;
        ss << line_vals;


        while (ss.good()) {
            char trash;
            ss >> val >> trash;
            vals.push_back(val);
        }

        for (size_t i = 0; i < tos.size(); i++) {
            addEdge(from, tos[i], vals[i]);
        }
    }
}

map<string, list<string>> Graph::DijsktraSP(const string& source) {
    struct props {
        int dist;
        string prev_visited;
        bool is_visited = false;
    };

    struct node_dist {
        string node;
        int dist {};

        node_dist() = default;
        node_dist(string n, int d) : node(std::move(n)), dist(d) {};
        bool operator <(const node_dist& other) const {
            return other.dist < dist; // azért van fordítva, hogy a queue csökkenőbe rendezzen, és a top() a legkisebb elem legyen
        }
    };

    unordered_map<string, props> matrix;
    priority_queue<node_dist> q;

    for (const auto& v : vertices) {
        matrix[v.first] = {std::numeric_limits<int>::max(), "", false};

        if (v.first == source) {
            matrix[v.first] = {0, "", false};
            q.push(node_dist(v.first, 0));
        }
    }

    node_dist curr;
    while(!q.empty()) {
        curr = q.top();
        q.pop();
        matrix[curr.node].is_visited = true;
        for (const auto& e : vertices[curr.node]->edges) {
            if (!matrix[e.first].is_visited) {
                if (curr.dist + e.second->val < matrix[e.first].dist) {
                    matrix[e.first].prev_visited = curr.node;
                    matrix[e.first].dist = curr.dist + e.second->val;
                }
                q.push(node_dist(e.first, matrix[e.first].dist));
            }
        }
    }
//    for (const auto& m : matrix) {
//        cout << m.first << " " << m.second.dist << " " << m.second.prev_visited << " " << m.second.is_visited << endl;
//    }

    map<string, list<string>> shortest_paths;

    for (const auto& m : matrix) {
        list<string> path;
        string curr_node = m.first;
        path.push_front(curr_node);
        while (curr_node != source)
        {
            curr_node = matrix[curr_node].prev_visited;
            path.push_front(curr_node);
        }
        shortest_paths[m.first] = path;
    }

    return shortest_paths;
}

[[maybe_unused]] void Graph::printDijsktraSP(const string& source) {
    map<string, list<string>> shortest_paths = DijsktraSP(source);

    cout << "Dijsktra's shortest paths: " << endl;
    for (const auto& s : shortest_paths) {
        for (const auto& node : s.second) {
            cout << node << " ";
        }
        cout << endl;
    }
}

Graph Graph::PrimMST() {
    unordered_set<string> visited_vertices {};
    unordered_set<string> unvisited_vertices {};
    priority_queue<Edge> edge_pool{};
    vector<Edge> result{};

    for (const auto& v : vertices) {
        unvisited_vertices.insert(v.first);
    }

    while (!unvisited_vertices.empty()) {
        if (visited_vertices.empty()) {
            visited_vertices.insert(*unvisited_vertices.begin());
            unvisited_vertices.erase(unvisited_vertices.begin());
            for (const auto& e : vertices[*visited_vertices.begin()]->edges) {
                edge_pool.push(*e.second);
            }

        }

        while (!edge_pool.empty()) {
            Edge edge = edge_pool.top();
            edge_pool.pop();

            if(visited_vertices.contains(edge.vertex1) && visited_vertices.contains(edge.vertex2)) {
                continue;
            }
            else {
                string node;
                if (unvisited_vertices.contains(edge.vertex1)) {
                    node = edge.vertex1;
                } else{
                    node = edge.vertex2;
                }

                result.push_back(edge);
                visited_vertices.insert(node);
                unvisited_vertices.erase(node);
                for (const auto& e : vertices[node]->edges){
                    edge_pool.push(*e.second);
                }
            }
        }
    }

    Graph tree;
    for (const auto& r : result) {
        tree.addEdge(r.vertex1, r.vertex2, r.val);
    }

    return tree;

//    while (!edge_pool.empty()) {
//        Edge edge = edge_pool.top();
//        edge_pool.pop();
//        cout << edge.vertex1 << " " << edge.vertex2 << " " << edge.val << endl;
//    }
}

[[maybe_unused]] void Graph::printPrimMST() {
    Graph tree = PrimMST();
    tree.print();
}

void Graph::removeEdge(const string& from, const string& to) {
    if (vertices.contains(from)) {
        if (!vertices[from]->edges.contains(to)) { // nem létező él
            cout << "There is no edge: " << from << "--" << to << endl;
            return;
        }
    }
    else {
        cout << "There is no edge: " << from << "--" << to << endl;
        return;
    }

    string independent_vertex;
    string other;

    if (vertices[from]->edges.size() == 1 && vertices[to]->edges.size() == 1) {
        clear();
        return;
    } else if (vertices[from]->edges.size() == 1){
        independent_vertex = from;
        other = to;
    } else if (vertices[to]->edges.size() == 1) {
        independent_vertex = to;
        other = from;
    }

    if (!independent_vertex.empty()) {
        Edge* edge = vertices[independent_vertex]->edges[other];
        vertices[independent_vertex]->edges.erase(other);
        vertices[other]->edges.erase(independent_vertex);
        edges.erase(*edge);
        delete edge;
        delete vertices[independent_vertex];
        vertices.erase(independent_vertex);
    } else {
        Graph h(*this);

        Edge* h_edge = h.vertices[from]->edges[to];
        h.vertices[from]->edges.erase(to);
        h.vertices[to]->edges.erase(from);
        h.edges.erase(*h_edge);
        delete h_edge;

        if (h.is_connected()) {
            Edge* edge = vertices[from]->edges[to];
            vertices[from]->edges.erase(to);
            vertices[to]->edges.erase(from);
            edges.erase(*edge);
            delete edge;
        }
        else {
            throw IndependentVertex(from, to);
        }
    }
}

bool Graph::is_connected() {
    unordered_map<string, unordered_map<string, bool>> adj = getAdjacencyMatrix();
    size_t n = adj.size();

    unordered_map<string, bool> unvisited_vertices;
    for (const auto &v : vertices) {
        unvisited_vertices[v.first] = false;
    }

    unvisited_vertices[vertices.begin()->first] = true;

    vector<string> queue(n);
    size_t head = 0;
    size_t tail = 0;
    queue[head] = vertices.begin()->first;

    // Begin Breadth-First Search Algorithm
    while (head <= tail){
        for (const auto &v : vertices) {
            if (queue[head] != v.first && adj[queue[head]][v.first] && !unvisited_vertices[v.first]) {
                tail++;
                queue[tail] = v.first;
                unvisited_vertices[v.first] = true;
            }
        }
        head++;
    }

    if (tail != n-1) {
        return false;
    }

    return true;
}

unordered_map<string, unordered_map<string, bool>> Graph::getAdjacencyMatrix() {
    unordered_map<string, unordered_map<string, bool>> adj {};
    for (const auto& v1 : vertices) {
        unordered_map<string, bool> tmp;
        for (const auto& v2 : vertices) {
            tmp[v2.first] = false;
            if (vertices[v1.first]->edges.contains(v2.first)) {
                tmp[v2.first] = true;
            }
        }
        adj[v1.first] = (tmp);
    }

    return adj;
}

[[maybe_unused]] void Graph::printAdjacencyMatrix() {
    unordered_map<string, unordered_map<string, bool>> adj = getAdjacencyMatrix();

    for (const auto& pair : adj) {
        if (pair == *adj.begin()) {
            cout << "  ";
            for (const auto& v2 : pair.second) {
                cout << v2.first << " ";
            }
            cout << endl;
        }
        cout << pair.first << " ";
        for (const auto& v2 : pair.second) {
            cout << v2.second << " ";
        }
        cout << endl;
    }
}

void Graph::clear() {
    for (const auto& v : vertices) {
        delete v.second;
    }
    vertices.clear();

    for (const auto& e : edges) {
        delete e.second;
    }
    edges.clear();
}

Graph::Graph(const Graph & other) {
    for (const auto& e : other.edges) {
        Edge* new_edge = new Edge(*e.second);
        edges[*e.second] = new_edge;
    }
    for (const auto& v : other.vertices) {
        Node* new_node = new Node(v.first);
        vertices[v.first] = new_node;
        for (const auto& e : v.second->edges) {
            vertices[v.first]->edges[e.first] = edges[*e.second];
        }
    }
}

Graph &Graph::operator=(const Graph &other) {
    clear();

    for (const auto& e : other.edges) {
        Edge* new_edge = new Edge(*e.second);
        edges[*e.second] = new_edge;
    }
    for (const auto& v : other.vertices) {
        Node* new_node = new Node(v.first);
        vertices[v.first] = new_node;
        for (const auto& e : v.second->edges) {
            vertices[v.first]->edges[e.first] = edges[*e.second];
        }
    }

    return *this;
}

void Graph::removeVertex(const string& vertex) {
    if (!vertices.contains(vertex)) { // nem létező csúcs
        cout << "There is no vertex: " << vertex << endl;
        return;
    }

    vector<string> to_remove;
    for (const auto& v : vertices[vertex]->edges) {
        to_remove.push_back(v.first);
    }

    for (const auto& t : to_remove) {
        removeEdge(vertex,t);
    }
}

bool Graph::Edge::operator==(const Graph::Edge &other) const {
    if ((vertex1 == other.vertex1 && vertex2 == other.vertex2 && val == other.val) || (vertex2 == other.vertex1 && vertex1 == other.vertex2 && val == other.val)){
        return true;
    }
    return false;
}

bool Graph::Edge::operator<(const Graph::Edge& other) const {
    return val > other.val; // reveresed for priority queue in PrimMST()
}

