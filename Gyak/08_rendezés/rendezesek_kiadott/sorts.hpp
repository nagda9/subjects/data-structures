#ifndef SORTS_HPP
#define SORTS_HPP

#include <cstddef>

/// Segéd függvények
void swap(int &a, int &b) { // Hagyományos cserélő függvény
  (void)a;
  (void)b;
  // TODO
}

/// ========== BUBORÉKRENDEZÉS ==========
void bubbleSort(int *a, int n) {
  (void)a;
  (void)n;
  // TODO
}

/// ========== MAXIMUM KIVÁLASZTÁSOS RENDEZÉS ==========
// A rendező Segédfüggvénye: megkeresi a maximális értékű elemet a megadott
// tartományban (j-ig)
int maxSel(const int *a, int j) {
  (void)a;
  (void)j;
  // TODO
  return 0;
}

void maxSort(int *a, int n) {
  (void)a;
  (void)n;
  // TODO
}

/// ========== BESZÚRÓ RENDEZÉS ==========
void insertionSort(int *a, int n) {
  (void)a;
  (void)n;
  // TODO
}

#endif
