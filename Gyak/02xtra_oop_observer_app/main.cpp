#include <iostream>
#include <memory>

#include "Application.h"
#include "BigBrother.h"

int main() {
    std::cout << "OOP observer app...\n";

    auto app = std::make_unique<app::Application>("BigBrother");
    auto bb = std::make_unique<BigBrother>();

    app->attach_layer(bb.get());

    app->run();

    return 0;
}
