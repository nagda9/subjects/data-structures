cmake_minimum_required(VERSION 3.14)
project(khf04)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -pedantic")

add_executable(khf04 main.cpp)
