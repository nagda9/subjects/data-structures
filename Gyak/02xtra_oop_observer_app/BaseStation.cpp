#include "BaseStation.h"

BaseStation::BaseStation(std::string _name, Position2D _pos) : name(_name), pos(_pos)
{

}

BaseStation::~BaseStation()
{

}

std::string BaseStation::getName() {
    return name;
}

Position2D BaseStation::getPos() {
    return pos;
}

double BaseStation::getTargetDistance() {
    return targetDistance;
}

void BaseStation::update(Position2D _pos) {
    this->targetDistance = sqrt((this->pos.x - _pos.x)*(this->pos.x - _pos.x) + (this->pos.y - _pos.y)*(this->pos.y - _pos.y));
    std::cout << "Subject distance from " << name << ": " << this->targetDistance << "\n";
}
