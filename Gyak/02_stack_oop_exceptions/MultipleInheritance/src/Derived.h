#ifndef DERIVED_H
#define DERIVED_H

#include "Base1.h"
#include "Base2.h"

class Derived: public Base1, public Base2 {
public:
	Derived();
	virtual ~Derived();
	void printAB();
protected:
private:
};

#endif // DERIVED_H
