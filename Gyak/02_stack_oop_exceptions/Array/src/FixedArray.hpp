#ifndef FIXEDARRAY_HPP_INCLUDED
#define FIXEDARRAY_HPP_INCLUDED

#include "AbstractArray.hpp"

class FixedArray: public AbstractArray {
public:
	FixedArray(int max_size);
	~FixedArray();

	int& operator[](int i);

	void push_back(int var);

protected:
	int* base_array;
};

#endif // FIXEDARRAY_HPP_INCLUDED
