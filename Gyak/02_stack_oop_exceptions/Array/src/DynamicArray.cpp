#include "DynamicArray.hpp"
#include <iostream>

DynamicArray::DynamicArray(int max_size) :
		AbstractArray(max_size) {
	if (max_size > 0)
		base_array = new int[max_size];
}

DynamicArray::~DynamicArray() {
	if (base_array != nullptr) {
		delete[] base_array;
	}
}

int& DynamicArray::operator[](int i) {
	return base_array[i];
}

void DynamicArray::push_back(int var) {
	if (act_size == max_size - 1) {
		max_size *= 2;
		int* temp = new int[max_size];
		for (int i = 0; i < act_size; i++) {
			temp[i] = base_array[i];
		}
		delete[] base_array;
		base_array = temp;
	}
	base_array[act_size++] = var;
}
