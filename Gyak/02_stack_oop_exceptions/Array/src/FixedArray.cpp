#include "FixedArray.hpp"
#include <iostream>

FixedArray::FixedArray(int max_size) :
		AbstractArray(max_size) {
	if (max_size > 0) {
		base_array = new int[max_size];
	}
}

FixedArray::~FixedArray() {
	if (base_array != nullptr) {
		delete[] base_array;
	}
}

int& FixedArray::operator[](int i) {
	return base_array[i];
}

void FixedArray::push_back(int var) {
	if (act_size < max_size)
		base_array[act_size++] = var;
}
