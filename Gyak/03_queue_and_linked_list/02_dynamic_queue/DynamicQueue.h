#ifndef DYNAMICQUEUE_H_VR0M8CGS
#define DYNAMICQUEUE_H_VR0M8CGS

#include <iostream>

#include "../exceptions/listexception.hpp"

template <typename T> class DynamicQueue {
public:
  DynamicQueue() : head{nullptr}, tail{nullptr} {}
  DynamicQueue(const DynamicQueue &other) = delete;
  DynamicQueue(DynamicQueue &&other) = delete;
  DynamicQueue &operator=(DynamicQueue rhs) = delete;
  DynamicQueue &operator=(DynamicQueue &&rhs) = delete;
  ~DynamicQueue();
  void in(T new_item);
  T out();
  T first() const;
  bool isEmpty() const;
  void print() const;

private:
  struct Node {
    T value;
    Node *pNext;

    Node() : value{}, pNext(nullptr) {}
    Node(T _value) : value(_value), pNext(nullptr) {}
    Node(T _value, Node *_pNext) : value(_value), pNext(_pNext) {}
  };

  Node *head, *tail;
};

/// ----- Most következnek a metódus implementációk ----- ///

template <typename T> DynamicQueue<T>::~DynamicQueue() {
  while (!isEmpty()) {
    out();
  }
}

template <typename T> bool DynamicQueue<T>::isEmpty() const {
  return head == nullptr;
}

template <typename T> void DynamicQueue<T>::in(T new_item) {
  Node *p = new Node(new_item);
  if (isEmpty()) {
    tail = p;
    head = p;
  } else {
    tail->pNext = p;
    tail = p;
  }
}

template <typename T> T DynamicQueue<T>::out() {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  T tmp = head->value;
  Node *p = head;
  head = head->pNext;
  delete p;
  return tmp;
}

template <typename T> T DynamicQueue<T>::first() const {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  return head->value;
}

template <typename T> void DynamicQueue<T>::print() const {
  for (Node *i = head; i != nullptr; i = i->pNext) {
    std::cout << i->value << " ";
  }
}

#endif /* end of include guard: DYNAMICQUEUE_H_VR0M8CGS */
