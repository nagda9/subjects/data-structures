#include "FixedQueue.h"
#include "../exceptions/exceptions.hpp"

FixedQueue::FixedQueue() {
  empty = true;
  head = tail = 0;
}

// FixedQueue::FixedQueue() : head(0), tail(0), empty(true) {} // igy is lehet

FixedQueue::~FixedQueue() {}

bool FixedQueue::isEmpty() const { return empty; }

bool FixedQueue::isFull() const { return (head == tail && !isEmpty()); }

void FixedQueue::in(int new_item) {
  if (isFull()) {
    throw OverFlowException();
  }

  empty = false;

  array[tail] = new_item;

  if (tail == CAPACITY - 1) {
    tail = 0;
  } else {
    tail++;
  }
}

int FixedQueue::out() {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  int tmp = array[head];

  if (head == CAPACITY - 1) {
    head = 0;
  } else {
    head++;
  }

  if (head == tail) {
    empty = true;
  }

  return tmp;
}

int FixedQueue::first() const {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  return array[head];
}

void FixedQueue::print() const {
  if (isEmpty()) {
    return;
  }

  if (head < tail) { // alap eset, amíg a head elött van a tail
    for (int i = head; i < tail - 1; i++) {
      std::cout << array[i] << ", ";
    }
    std::cout << array[tail - 1] << std::endl;
  } else { // ha már körbe fordult a tail
           // akkor head-től a tömb végéig (ami valahol a sor közepén van)
    for (int i = head; i < CAPACITY; i++) {
      std::cout << array[i] << ", ";
    }
    for (int i = 0; i < tail; i++) { /// majd előről a tail-ig
      std::cout << array[i] << ", ";
    }
  }
}
