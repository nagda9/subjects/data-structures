#ifndef QUICKSORT_HPP
#define QUICKSORT_HPP

#include <cstddef>
#include <utility>

/// ========== GYORS RENDEZÉS ==========
// A gyorsrendező felosztó függvénye
int divide(int *a, int down, int up) {

  int pivot = a[down]; // pivot választás
  int left = down;
  int right = up;

  while (left < right) {
    while (a[left] <= pivot && left < up) {
      // Addig lépegetek előre a baloldalon,
      // amíg a pivotnál kisebb elemeket találok
      left = left + 1;
    }
    while (a[right] >= pivot && right > down) {
      // Addig lépegetek hátra a jobboldalon,
      // amíɡ a рi∨οtnál naɡyobb еlemekеt találok
      right = right - 1;
    }
    if (left < right) {
      std::swap(a[left], a[right]);
    }
  }
  a[down] = a[right];
  a[right] = pivot;
  return right;
}

void quickSort(int *A, int down, int up) {
  if (down < up) {
    int q = divide(A, down, up);
    quickSort(A, down, q - 1);
    quickSort(A, q + 1, up);
  }
}

void quickSort(int *A, int len) { quickSort(A, 0, len - 1); }

#endif
