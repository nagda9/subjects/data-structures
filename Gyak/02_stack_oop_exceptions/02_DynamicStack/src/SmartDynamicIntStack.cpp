#include "SmartDynamicIntStack.hpp"
#include "exceptions.hpp"
#include <iostream>

SmartDynamicIntStack::SmartDynamicIntStack(const SmartDynamicIntStack &other) {
    if(other.pHead) {
        pHead = std::make_unique<Node>(other.pHead->value);
        Node *copied = pHead.get();
        for (Node *i = other.pHead->pNext.get(); i != nullptr; i = i->pNext.get()) {
            copied->pNext = std::make_unique<Node>(i->value);
            copied = copied->pNext.get();
        }
    }
}

SmartDynamicIntStack& SmartDynamicIntStack::operator= (const SmartDynamicIntStack& rhs) {
    if(this != &rhs) {
        while (!isEmpty()) {
            pop();
        }
        if(rhs.pHead) {
            pHead = std::make_unique<Node>(rhs.pHead->value);
            Node *copied = pHead.get();
            for (Node *i = rhs.pHead->pNext.get(); i != nullptr; i = i->pNext.get()) {
                copied->pNext = std::make_unique<Node>(i->value);
                copied = copied->pNext.get();
            }
        }
    }
    return *this;
}

bool SmartDynamicIntStack::isEmpty() const {
    return (pHead == nullptr);
}

void SmartDynamicIntStack::push(int new_item) {
    std::unique_ptr<Node> p = std::make_unique<Node>(new_item);
    p->pNext = std::move(pHead);
    pHead = std::move(p);
}

int SmartDynamicIntStack::top() const {
    if (isEmpty()) {
        throw UnderFlowException();
    }

    return pHead->value;
}

int SmartDynamicIntStack::pop() {
    if (isEmpty()) {
        throw UnderFlowException();
    }

    int tmp = pHead->value;
    pHead = std::move(pHead->pNext);
    return tmp;
}

void SmartDynamicIntStack::print() const {
    for (Node* i = pHead.get(); i != nullptr; i = i->pNext.get()) {
        std::cout << i->value << " ";
    }
}
