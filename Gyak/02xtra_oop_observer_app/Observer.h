#ifndef OBSERVER_H
#define OBSERVER_H

#include "Position2D.h"

class Observer
{
    public:
        virtual void update(Position2D) = 0;
};

#endif // OBSERVER_H
