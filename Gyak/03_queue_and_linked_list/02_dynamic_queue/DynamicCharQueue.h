#ifndef DYNAMICCHARQUEUE_H_
#define DYNAMICCHARQUEUE_H_

class DynamicCharQueue {
public:
  DynamicCharQueue() : head{nullptr}, tail{nullptr} {}
  DynamicCharQueue(const DynamicCharQueue &) = delete;
  DynamicCharQueue(DynamicCharQueue &&) = delete;
  // FIXME: const?
  const DynamicCharQueue &operator=(const DynamicCharQueue &) = delete;
  const DynamicCharQueue &operator=(DynamicCharQueue &&) = delete;
  ~DynamicCharQueue();
  void in(char new_item);
  char out();
  char first() const;
  bool isEmpty() const;
  void print() const;

private:
  class Node {
  public:
    char value;
    Node *pNext;

    Node() : value(0), pNext(nullptr) {}
    Node(const char &_value) : value(_value), pNext(nullptr) {}
    Node(const char &_value, Node *_pNext) : value(_value), pNext(_pNext) {}
  };

  Node *head, *tail;
};

#endif /* DYNAMICCHARQUEUE_H_ */
