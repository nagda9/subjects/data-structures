//============================================================================
// Name        : main.cpp
//============================================================================

#include "utility.h"
#include <iostream>
using namespace std;

int main() {
    //1. ora 1. demonstracio - Valtozok
    // Ez a program demonstralja a valtozok es a memoria kapcsolatat

    // Minden memóriacím rendelkezhet:
        // címmel (mindig)
        // változó névvel (ha a memóriacím lefoglalt, a változó deklarált)
        // típussal (ha a memóriacím lefoglalt, a változó deklarált)
        // értékkel (ha a változó definiált)
        // referált értékkel
            // amire a memóriacím értéke mutat
            // (CSAK pointer esetén)
    // A referencia típus annyit csinál, hogy egy memóriacímhez egy új változónevet rendel
    // A pointer típus egy dologgal többet tud, mint egy sima változó, mivel van egy referált értéke

    // A referenciának mindig egy változót adunk át
    // A pointernek mindig memóriacímet adunk át

    int i = 5;
    // Valtozo sajat memoriaterulettel
    // Az i memoriacime '&i == 0x7ffc4f3831d4', tipusa 'type_name<decltype(i)>() == int'
    // es a memoriaterulet tartalma 'i == 5'

    int& r = i;
    // Referencia típus az előző változóra
    // Az i memoriacime '&r == &i == 0x7ffc4f3831d4', tipusa 'type_name<decltype(i)>() == int&'
    // es a memoriaterulet tartalma 'r == 5'

    int* p = &i;
    // Pointer típus az elozo memoriateruletre
    // A  p memoriacime '&p == 0x7ffc4f3831d8', tipusa 'type_name<decltype(p)>() == int*'
    // es a memoriaterulet tartalma 'p == 0x7ffc4f3831d4'
    // A  p altal hivatkozott memoriaterulet cime  '(&(*p)) == 0x7ffc4f3831d4',
    // tipusa 'type_name<decltype(*p)>() == int&' es annak tartalma '(*p) == 5'

    // Vegyuk eszre, hogy az eredeti valtozo es a referencia ugyanazon a cimen dolgozik
    // Vegyuk eszre, hogy a pointer altal tarolt cim pontosan az a cim, ami az i valtozo cime
    // Tovabba, nezzuk meg azt is, hogy a pointer teljesen mas cimen talalhato

    /// Valtoztassuk meg az ertekeket:
    i++;
    // Az i memoriacime '&i == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'i == 6'
    // Az r memoriacime '&r == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'r == 6'
    // Az p memoriacime '&p == 0x7ffc4f3831d8' es a memoriaterulet tartalma 'p == 0x7ffc4f3831d4'
    // A  p altal hivatkozott memoriaterulet cime '(&(*p)) == 0x7ffc4f3831d4' es annak tartalma '(*p) == 6'

    r++;
    // Az i memoriacime '&i == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'i == 7'
    // Az r memoriacime '&r == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'r == 7'
    // A  p memoriacime '&p == 0x7ffc4f3831d8' es a memoriaterulet tartalma 'p == 0x7ffc4f3831d4'
    // A  p altal hivatkozott memoriaterulet cime '(&(*p)) == 0x7ffc4f3831d4' es annak tartalma '(*p) == 7'

    (*p)++;
    // Az i memoriacime '&i == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'i == 8'
    // Az r memoriacime '&r == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'r == 8'
    // A  p memoriacime '&p == 0x7ffc4f3831d8' es a memoriaterulet tartalma 'p == 0x7ffc4f3831d4'
    // A  p altal hivatkozott memoriaterulet cime '(&(*p)) == 0x7ffc4f3831d4' es annak tartalma '(*p) == 8'

    /// FIGYELEM!! MOST JON EGY SZOKASOS HIBA, ELRETTENTO PELDANAK:
    p++;
    // Itt a memóriacímet változtatjuk meg
    // Az i memoriacime '&i == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'i == 8'
    // Az r memoriacime '&r == 0x7ffc4f3831d4' es a memoriaterulet tartalma 'r == 8'
    // A  p memoriacime '&p == 0x7ffc4f3831d8' es a memoriaterulet tartalma *****'p == 0x7ffc4f3831d8'******
    // A  p altal hivatkozott memoriaterulet cime '(&(*p)) == 0x7ffc4f3831d8' es annak tartalma '(*p) == 1329082840'

    //  Nezzuk meg, hogy az uj terulet mogotti dolgokat meg lehet-e valtoztatni!
    (*p)++;
    // A memoriateruletet (erteket) valtoztattuk meg
    // A p memoriacime '&p == 0x7ffc4f3831d8' es a memoriaterulet tartalma 'p == 0x7ffc4f3831d9'
    // A p altal hivatkozott memoriaterulet cime '(&(*p)) == 0x7ffc4f3831d9' es annak tartalma '(*p) == -61917135'
    // Ne feledjuk, c++-ban alapvetoen mindent lehet, kiveve par dolgot amit tilos!

    return 0;
}
