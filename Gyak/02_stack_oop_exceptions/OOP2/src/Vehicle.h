#ifndef VEHICLE_H
#define VEHICLE_H

using namespace std;

class Vehicle {

    protected:

    string type;
    double kmh;
    int capacity;

    public:

    Vehicle(){}

    Vehicle(string type, double kmh, int capacity) : type(type), kmh(kmh), capacity(capacity) {}

    virtual double moveOn(double km) = 0;

    string getType() {
        return type;
    }

    double getKmh() {
        return kmh;
    }

    int getCapacity() {
        return capacity;
    }

};

#endif /*VEHICLE_H*/
