#ifndef BASESTATION_H
#define BASESTATION_H

#include <iostream>
#include <string>

#include "Position2D.h"
#include "Observer.h"
#include <math.h>

class BaseStation : Observer
{
    public:
        BaseStation(std::string, Position2D);
        virtual ~BaseStation();
        std::string getName();
        Position2D getPos();
        double getTargetDistance();

        void update(Position2D);
    protected:
    private:
        std::string name;
        Position2D pos;
        double targetDistance = 0;
};

#endif // BASESTATION_H
