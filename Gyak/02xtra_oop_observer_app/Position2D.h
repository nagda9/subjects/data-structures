#ifndef POSITION2D_H
#define POSITION2D_H


class Position2D
{
    public:
        Position2D() = default;
        Position2D(int, int);
        void setPosition(int, int);

        int x = 0;
        int y = 0;
};

#endif // POSITION2D_H
