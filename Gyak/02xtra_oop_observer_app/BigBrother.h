//
// Created by Balazs Nagy on 2020. 09. 12..
//

#ifndef OOP_OBSERVER_APP_BIGBROTHER_H
#define OOP_OBSERVER_APP_BIGBROTHER_H

#include "Layer.h"

#include <iostream>
#include <vector>
#include <string>

#include "Mobile.h"
#include "Notebook.h"
#include "BaseStation.h"
#include "Position2D.h"

class BigBrother : public app::Layer {
    std::vector<BaseStation> stations;
    std::string names [5] = {"Debrecen", "Budapest", "Wien", "Praha", "Milano"};

    std::vector<Subject*> subjects;
public:
    BigBrother();
    ~BigBrother();

    void on_update() override;
};


#endif //OOP_OBSERVER_APP_BIGBROTHER_H
