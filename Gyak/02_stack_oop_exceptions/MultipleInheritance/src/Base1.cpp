#include "Base1.h"
#include <iostream>

Base1::Base1() :
		a("protected string Base1::a") {
	std::cout << "Constructor Base1()" << std::endl;
}

Base1::~Base1() {
	std::cout << "Destructor ~Base1()" << std::endl;
}

void Base1::printA() {
	std::cout << a << std::endl;
}

void Base1::f() {
	std::cout << "Base1::f()" << std::endl;
}
