cmake_minimum_required(VERSION 3.14)
project(fixed_queue)

set(CMAKE_CXX_STANDARD 20)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Werror -Wall -Wextra -pedantic")

add_executable(fixed_queue FixedQueue.cpp fixed_queue_main.cpp FixedQueue.h)
