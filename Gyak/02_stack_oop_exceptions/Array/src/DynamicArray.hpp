#ifndef ARRAY_HPP_INCLUDED
#define ARRAY_HPP_INCLUDED

#include "AbstractArray.hpp"

class DynamicArray: public AbstractArray {
public:
	DynamicArray(int max_size);
	~DynamicArray();

	int& operator[](int i);

	void push_back(int var);

protected:
	int* base_array;
};

#endif // ARRAY_HPP_INCLUDED
