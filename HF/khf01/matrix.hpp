#ifndef ADATALG_KHF01_MATRIX_HPP
#define ADATALG_KHF01_MATRIX_HPP
#include <iostream>
#include "filesystem"
#include <fstream>

using namespace std;

class matrix {
private:
    size_t n{}, m{};
    int* mat; // dinamikus tömb
public:
    explicit matrix(std::filesystem::path);
    matrix(const size_t&, const size_t&);
    ~matrix();
    [[nodiscard]] matrix operator+(const matrix &) const noexcept;
    [[nodiscard]] matrix operator*(const matrix &) const noexcept;
    [[nodiscard]] int& at(size_t, size_t);
    [[nodiscard]] const int& at(size_t, size_t) const;
    [[nodiscard]] std::pair<size_t, size_t> size() const;
    void transpose() noexcept;
    void print() const noexcept;
    [[nodiscard]] size_t colsub2ind(const size_t&, const size_t&) const; // oszlopfolytonosan indexelt mátrixból vektor index
    /* |11|12|13| -> |0|2|4|
     * |21|22|23|    |1|3|5|
     */
    [[nodiscard]] size_t rowsub2ind(const size_t&, const size_t&) const; // sorfolytonosan indexelt mátrixból vektor index
    /* |11|12|13| -> |0|1|2|
     * |21|22|23|    |3|4|5|
     */
    matrix(const matrix&);
    matrix& operator=(const matrix&);
};

matrix::matrix(const size_t& row, const size_t& col) : n(row), m(col) {
    mat = new int[n*m];
}

size_t matrix::colsub2ind(const size_t& i, const size_t& j) const {
    return j*n+i;
}

size_t matrix::rowsub2ind(const size_t& i, const size_t& j) const {
    return i*m+j;
}

matrix::matrix(std::filesystem::path path) {
    ifstream bf;
    if (path.is_absolute())
    {
        bf.open(path);
    }
    else
    {
        string path_string{path.string()};
        bf.open("../" + path_string);
    }
    if (!bf.good())
    {
        cerr << "Could not open file!" << endl;
    }

    bf >> n >> ws >> m >> ws;

    mat = new int[n*m];
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            size_t ind = colsub2ind(i, j);
            bf >> mat[ind];
        }
    }
    bf.close();
}

matrix::~matrix() {
    delete[] mat;
}

void matrix::print() const noexcept {
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            size_t ind = colsub2ind(i, j);
            cout << mat[ind] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

void matrix::transpose() noexcept {
    matrix new_mat(m, n);
    for (size_t i = 0; i < m; i++) {
        for (size_t j = 0; j < n; j++) {
            size_t ind_new = new_mat.colsub2ind(i, j);
            size_t ind_old = colsub2ind(j, i);
            new_mat.mat[ind_new] = mat[ind_old];
        }
    }
    *this = new_mat;
    /// ehhez fel kell szabadítani a mat-ot az assignment operátorban, hogy ne legyen memori leak
}

std::pair<size_t, size_t> matrix::size() const {
    return pair<size_t, size_t> {n, m};
}

int& matrix::at(size_t x, size_t y) {
    if (x < n && y < m) {
        size_t ind = colsub2ind(x, y);
        return mat[ind];
    }
    else {
        throw out_of_range("Invalid index!");
    }
}

const int& matrix::at(size_t x, size_t y) const {
    if (x < n && y < m) {
        size_t ind = colsub2ind(x, y);
        return mat[ind];
    }
    else {
        throw out_of_range("Invalid index!");
    }
}

matrix matrix::operator*(const matrix& other) const noexcept {
    matrix result(this->n, other.m);
    for (size_t i = 0; i < this->n; i++) {
        for (size_t j = 0; j < other.m; j++) {
            int sum = 0;
            for (size_t k = 0; k < this->m; k++) {
                size_t this_ind = this->colsub2ind(i, k);
                size_t other_ind = other.colsub2ind(k, j);
                sum += this->mat[this_ind]*other.mat[other_ind];
            }
            size_t result_ind = result.colsub2ind(i, j);
            result.mat[result_ind] = sum;
        }
    }
    return result;
}

matrix matrix::operator+(const matrix & other) const noexcept {
    matrix result(this->n, this->m);
    for (size_t i = 0; i < this->n; i++) {
        for (size_t j = 0; j < this->m; j++) {
            size_t result_ind = result.colsub2ind(i, j);
            size_t this_ind = this->colsub2ind(i, j);
            size_t other_ind = other.colsub2ind(i, j);
            result.mat[result_ind] = this->mat[this_ind] + other.mat[other_ind];
        }
    }
    return result;
}

matrix::matrix(const matrix& other) {
    n = other.n;
    m = other.m;
    mat = new int[n*m];
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < m; j++) {
            size_t ind = other.colsub2ind(i, j);
            mat[ind] = other.mat[ind];
        }
    }
}

matrix& matrix::operator=(const matrix &other) {
    if (this != &other) {
        delete[] mat;

        n = other.n;
        m = other.m;
        mat = new int[n*m];
        for (size_t i = 0; i < n; i++) {
            for (size_t j = 0; j < m; j++) {
                size_t ind = other.colsub2ind(i, j);
                mat[ind] = other.mat[ind];
            }
        }
    }
    return *this;
}

#endif //ADATALG_KHF01_MATRIX_HPP
