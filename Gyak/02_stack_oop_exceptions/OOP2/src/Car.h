#ifndef CAR_H
#define CAR_H

#include "Vehicle.h"

using namespace std;

class Car: public Vehicle {

protected:

	//peldanyvaltozok

	double consumption;
	double petrol;

	//peldanymetodus, mely nem tartozik a publikus interface-hez
	double petrolNeed(double km) {
		return ((double) km / 100) * consumption;
	}

public:

	Car() {
	}

	virtual ~Car() {
	}

	//osztalyvaltozo
	static int petrolCost;

	//konstruktorok

	Car(string type, double consumption, int capacity, int kmh = 0) :
			Vehicle(type, kmh, capacity), consumption(consumption) {
		petrol = 0;
	}

	//osztalymetodus

	static int getCost(int litre) {
		return petrolCost * litre;
	}

	//peldanymetodusok

	virtual void refuel(double litre) {
		petrol += litre;
	}

	virtual double cost(double km) {
		return petrolNeed(km) * petrolCost;
	}

	virtual double moveOn(double km) {
		if (petrolNeed(km) <= petrol) {
			petrol -= petrolNeed(km);
			kmh += km;
			cout << "Megtettunk " << km << " km utat!\n";
			return km;
		} else {
			return 0;
		}
	}

	//getterek

	double getConsumption() {
		return consumption;
	}

	double getPetrol() {
		return petrol;
	}

};

int Car::petrolCost = 250;

#endif /*CAR_H*/
