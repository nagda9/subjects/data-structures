#include <iostream>
#include "Derived.h"

using namespace std;

int main() {
	cout << "Multiple Inheritance" << endl;

	Derived d;
	d.printA();
	d.printB();
	d.printAB();
	d.Base1::f();
	d.Base2::f();
	return 0;
}
