#include "adakrsetz_sort.hpp"
#include "woodpecker.hpp"
#include <chrono>
#include <iostream>
#include <random>
#include <vector>

using real_sec = std::chrono::duration<double, std::ratio<1>>;

TEST("Sort small") {
  {
    // Üres tömb
    std::vector<int> test_array{};
    adakrsetz_sort(test_array.data(), 0);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // Egy elem
    std::vector<int> test_array{1};
    adakrsetz_sort(test_array.data(), 1);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // Két elem, rendezett
    std::vector<int> test_array{1, 4};
    adakrsetz_sort(test_array.data(), 2);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // Két elem, rendezetlen
    std::vector<int> test_array{5, 2};
    adakrsetz_sort(test_array.data(), 2);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // Kevés elem
    std::vector<int> test_array{1, 3, 4, 2, 0, -1};
    adakrsetz_sort(test_array.data(), 6);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // Végéről visszavinni az első elemet
    std::vector<int> test_array{2, 3, 4, 5, 1};
    adakrsetz_sort(test_array.data(), 5);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // 3 elem, nem kell felosztani
    std::vector<int> test_array{2, 1, 3};
    adakrsetz_sort(test_array.data(), 3);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  {
    // 3 elem, fel kell osztani
    std::vector<int> test_array{2, 3, 1};
    adakrsetz_sort(test_array.data(), 3);
    CHECK_EQ(true, is_sorted(test_array.begin(), test_array.end()));
  }

  std::vector<int> v = {32, 8, 3, 44, 9, 5, 25, 24, 7, 12, 1, 22, 6, 4, 2};
  adakrsetz_sort(v.data(), v.size());
  CHECK_EQ(true, is_sorted(v.begin(), v.end()));
}

TEST("Better than quicksort on small sets") {
  constexpr unsigned N = 32;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist(0, N);
  std::vector<int> init(N);
  // total times in seconds
  double quicksort_total_time = 0.0, solution_total_time = 0.0;
  for (int i = 0; i < 10000; ++i) {
    for (unsigned j = 0; j < N; ++j) {
      init[j] = dist(gen);
    }
    std::vector<int> sortArray = init;
    TIMER(quicksort_short) {
      quickSort(sortArray.data(), 0, sortArray.size() - 1);
    }
    quicksort_total_time +=
        std::chrono::duration_cast<real_sec>(quicksort_short).count();
    sortArray = init;
    TIMER(solution_short) {
      adakrsetz_sort(sortArray.data(), sortArray.size());
    }
    solution_total_time +=
        std::chrono::duration_cast<real_sec>(solution_short).count();
  }
  std::cout << "quicksort: " << quicksort_total_time << "s\n";
  std::cout << "solution: " << solution_total_time << "s\n";
  CHECK_EQ(true, quicksort_total_time > solution_total_time);
}

TEST("Better than quicksort on normal sets") {
  constexpr unsigned N = 1 << 15;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist(0, N);
  std::vector<int> init(N);
  // total times in seconds
  double quicksort_total_time = 0.0, solution_total_time = 0.0;
  for (int i = 0; i < 200; ++i) {
    for (unsigned j = 0; j < N; ++j) {
      init[j] = dist(gen);
    }
    std::vector<int> sortArray = init;
    TIMER(quicksort_medium) {
      quickSort(sortArray.data(), 0, sortArray.size() - 1);
    }
    quicksort_total_time +=
        std::chrono::duration_cast<real_sec>(quicksort_medium).count();
    sortArray = init;
    TIMER(solution_medium) {
      adakrsetz_sort(sortArray.data(), sortArray.size());
    }
    solution_total_time +=
        std::chrono::duration_cast<real_sec>(solution_medium).count();
  }
  std::cout << "quicksort: " << quicksort_total_time << "s\n";
  std::cout << "solution: " << solution_total_time << "s\n";
  CHECK_EQ(true, quicksort_total_time > solution_total_time);
}

TEST("No worstcase") {
  constexpr unsigned N = 1 << 13;
  std::mt19937 gen(42);
  std::uniform_int_distribution<int> dist(0, N);
  std::vector<int> init(N);
  for (unsigned j = 0; j < N; ++j) {
    init[j] = j;
  }
  // total times in seconds
  double quicksort_total_time = 0.0, solution_total_time = 0.0;
  for (int i = 0; i < 5; ++i) {
    std::vector<int> sortArray = init;
    TIMER(quicksort_short) {
      quickSort(sortArray.data(), 0,
                sortArray.size() - 1); // Ehhez a teszthez a quicksortban mindig
                                       // az elso elem legyen a pivot.
    }
    quicksort_total_time +=
        std::chrono::duration_cast<real_sec>(quicksort_short).count();
    sortArray = init;
    TIMER(solution_short) {
      adakrsetz_sort(sortArray.data(), sortArray.size());
    }
    solution_total_time +=
        std::chrono::duration_cast<real_sec>(solution_short).count();
  }
  std::cout << "quicksort: " << quicksort_total_time << "s\n";
  std::cout << "solution: " << solution_total_time << "s\n";
  CHECK_EQ(true, quicksort_total_time > solution_total_time);
}

WOODPECKER_MAIN()
