#include "DynamicIntQueue.h"
#include <exception>
#include <iostream>

using namespace std;

int main() {
  cout << "Test the Dynamic Queue" << endl;

  DynamicIntQueue queue;

  // empty queue
  cout << "\n/**********Empty Queue**********\\" << endl;
  cout << "isEmpty(): " << queue.isEmpty() << endl; // 1 (true)
  cout << "print(): ";
  queue.print();
  cout << endl;

  try {
    cout << "first(): ";
    cout << queue.first() << endl; // exception
  } catch (exception &e) {
    cout << e.what() << endl;
  }
  try {
    cout << "out(): ";
    cout << queue.out() << endl; // exception
  } catch (exception &e) {
    cout << e.what() << endl;
  }

  // in & check
  cout << "\n/**********In to Queue**********\\" << endl;
  try {
    queue.in(1);
    queue.in(2);
    queue.in(3);
    queue.in(4);
    queue.in(5);

    cout << "After 5 items: ";
    queue.print();
    cout << endl;

    queue.in(6);
    queue.in(7);
    queue.in(8);
    queue.in(9);
    queue.in(10);

    cout << "After 10 items: ";
    queue.print();
    cout << endl;
    cout << "isEmpty(): " << queue.isEmpty() << endl; // 0 (false)

    queue.in(11);

    cout << "After 11 items: ";
    queue.print();
    cout << endl;
  } catch (exception &e) {
    cout << e.what() << endl;
  }

  // out
  cout << "\n/**********Out from Queue**********\\" << endl;
  try {
    cout << "first(): " << queue.first() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "After 5 items: ";
    queue.print();
    cout << endl;
    cout << "first(): " << queue.first() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;
    cout << "out(): " << queue.out() << endl;

    cout << "out(): " << queue.out() << endl; // exception
  } catch (exception &e) {
    cout << e.what() << endl;
  }

  // copy
  cout << "\n/**********Copy/Move**********\\" << endl;
  {
    DynamicIntQueue queue2(queue);
    cout << "Copy empty:" << endl;
    cout << "Queue 1: ";
    queue.print();
    cout << "\nQueue 2: ";
    queue2.print();
    cout << endl;

    cout << "Copy something:" << endl;
    queue.in(10);
    queue.in(11);
    queue.in(12);
    DynamicIntQueue queue3(queue);
    queue2 = queue;
    cout << "Queue 1: ";
    queue.print();
    cout << "\nQueue 2: ";
    queue2.print();
    cout << "\nQueue 3: ";
    queue3.print();
    cout << endl;
  }

  queue.out();
  queue.out();
  queue.out();
  // Move
  {
    DynamicIntQueue queue_temp{};
    DynamicIntQueue queue2(std::move(queue_temp));
    cout << "Move empty:" << endl;
    cout << "Temporary queue: ";
    queue_temp.print();
    cout << "\nTarget queue: ";
    queue2.print();
    cout << endl;

    DynamicIntQueue queue_temp2, queue_temp3{};
    cout << "Move something:" << endl;
    queue_temp2.in(20);
    queue_temp2.in(21);
    queue_temp2.in(22);
    queue_temp3.in(30);
    queue_temp3.in(31);
    queue_temp3.in(32);
    DynamicIntQueue queue3(std::move(queue_temp3));
    queue2 = std::move(queue_temp2);
    cout << "Temporary queues:" << endl;
    queue_temp.print();
    cout << endl;
    queue_temp2.print();
    cout << "\nQueue 2: ";
    queue2.print();
    cout << "\nQueue 3: ";
    queue3.print();
    cout << endl;
  }

  return 0;
}

