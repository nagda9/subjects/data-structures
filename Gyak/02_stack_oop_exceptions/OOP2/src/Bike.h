#ifndef BIKE_H
#define BIKE_H

#include "Vehicle.h"

using namespace std;

class Bike: public Vehicle {

public:

	Bike() {
	}

	Bike(string type, double kmh = 0, int capacity = 1) :
			Vehicle(type, kmh, capacity) {
	}

	double moveOn(double km) {
		kmh += km;
		cout << "Megtettunk " << km << " km utat." << endl;
		return km;
	}
};

#endif /*BIKE_H*/
