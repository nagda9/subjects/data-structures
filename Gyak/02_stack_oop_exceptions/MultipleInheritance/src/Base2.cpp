#include "Base2.h"

#include <iostream>

Base2::Base2() :
		b("protected string Base2::b") {
	std::cout << "Constructor Base2()" << std::endl;
}

Base2::~Base2() {
	std::cout << "Destructor ~Base2()" << std::endl;
}

void Base2::printB() {
	std::cout << b << std::endl;
}

void Base2::f() {
	std::cout << "Base2::f()" << std::endl;
}
