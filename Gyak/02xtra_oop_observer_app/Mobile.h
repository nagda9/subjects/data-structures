#ifndef MOBILE_H
#define MOBILE_H

#include <iostream>
#include <string>

#include "Position2D.h"
#include "Subject.h"

class Mobile : public Subject
{
    public:
        Mobile(std::string, Position2D);
        virtual ~Mobile();
        std::string getName() override;
        void moveOtherPosition(Position2D) override;
};

#endif // MOBILE_H
