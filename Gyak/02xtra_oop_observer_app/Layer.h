//
// Created by Balazs Nagy on 2020. 09. 12..
//

#ifndef OOP_OBSERVER_APP_LAYER_H
#define OOP_OBSERVER_APP_LAYER_H

namespace app {

    class Layer {
    public:
        virtual ~Layer() = default;
        virtual void on_update() = 0;
    };
}

#endif //OOP_OBSERVER_APP_LAYER_H
