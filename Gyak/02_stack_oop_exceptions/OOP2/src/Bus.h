#ifndef BUS_H
#define BUS_H

#include "Taxi.h"

using namespace std;

class Bus: public Taxi {

	//a pocket a vallalat penzet jeloli, a kmhCost pedig a sofor beret kilometerenkent

public:

	//osztalyvaltozo

	static int ticketCost;

	//konstruktor

	Bus() {
	}

	Bus(string type, double consumption, int capacity, double kmhCost) :
			Taxi(type, consumption, capacity, kmhCost) {
	}

	double carriage(double km) {
		if (moveOn(km) == km) {
			pocket += ticketCost - kmhCost * km;
			return km;
		} else
			return 0;
	}

	double carriage(double km, int member) {
		if (moveOn(km) == km) {
			pocket += member * ticketCost - km * kmhCost;
			return km;
		} else {
			return 0;
		}
	}

	double memberCost(double /*km*/, int /*member*/) {
		return ticketCost;
	}

	double profit(double km, int member) {
		return member * ticketCost - cost(km);
	}

	static double getTicketCost() {
		return ticketCost;
	}

};

int Bus::ticketCost = 1000;

#endif /*BUS_H*/
