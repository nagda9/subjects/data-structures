#include "textstat.hpp"

#include "woodpecker.hpp"
#include <algorithm>
#include <fstream>
#include <iterator>

// If you are lazy setting the working directory, hardcode the file locations
// here.
const std::string AUX_DIR = "./files/";

TEST("ctor, size & at", 1) {
  const TextStat ts(AUX_DIR + "small1.txt");
  CHECK_EQ(3, ts.size());
  CHECK_EQ(1, ts.at("c"));
  CHECK_EQ(2, ts.at("a"));
  CHECK_EQ(3, ts.at("b"));
}

TEST("empty input", 1) {
  const TextStat ts(AUX_DIR + "empty.txt");
  CHECK_EQ(0, ts.size());
}

TEST("union", 1) {
  const TextStat ts1(AUX_DIR + "small1.txt");
  const TextStat ts2(AUX_DIR + "small2.txt");
  const TextStat ts3 = ts1 | ts2;
  CHECK_EQ(4, ts3.size());
  CHECK_EQ(3, ts3.at("a"));
  CHECK_EQ(6, ts3.at("b"));
  CHECK_EQ(1, ts3.at("c"));
  CHECK_EQ(1, ts3.at("d"));
}

TEST("intersection", 1) {
  const TextStat ts1(AUX_DIR + "small1.txt");
  const TextStat ts2(AUX_DIR + "small2.txt");
  const TextStat ts3 = ts1 & ts2;
  CHECK_EQ(2, ts3.size());
  CHECK_EQ(1, ts3.at("a"));
  CHECK_EQ(3, ts3.at("b"));
}

TEST("serialize", 1) {
  const TextStat ts(AUX_DIR + "small1.txt");
  CHECK_EQ("{{a,2},{b,3},{c,1}}", ts.serialize());
}

TEST("summarize", 1) {
  const TextStat ts(AUX_DIR + "small3.txt");
  CHECK_EQ("{{1,[d]},{2,[a,b,c,f]},{3,[e,g]}}", ts.summarize());
}

TEST("small random", 1) {
  const TextStat ts(AUX_DIR + "small_rand.txt");
  CHECK_EQ(5, ts.size());
  CHECK_EQ("{{isxrm,4},{jiqfo,3},{tkgnk,8},{tqgxz,4},{zjxfd,6}}",
           ts.serialize());
  CHECK_EQ("{{3,[jiqfo]},{4,[isxrm,tqgxz]},{6,[zjxfd]},{8,[tkgnk]}}",
           ts.summarize());
}

TEST("large random", 1) {
  std::string serialized, summary;
  MEASURE("large random", 1s) {
    const TextStat ts(AUX_DIR + "large_rand.txt");
    CHECK_EQ(1000, ts.size());
    CHECK_EQ(318, ts.at("laaiphkdjnvfnbcewnzcyznicasgsw"));
    serialized = ts.serialize();
    summary = ts.summarize();
  }
  std::ifstream serialized_ref(AUX_DIR + "large_rand.serialized");
  CHECK_EQ(true, serialized_ref.good());
  CHECK_EQ(true, std::equal(serialized.begin(), serialized.end(),
                            std::istream_iterator<char>(serialized_ref),
                            std::istream_iterator<char>()));

  std::ifstream summary_ref(AUX_DIR + "large_rand.summary");
  CHECK_EQ(true, summary_ref.good());
  CHECK_EQ(true, std::equal(summary.begin(), summary.end(),
                            std::istream_iterator<char>(summary_ref),
                            std::istream_iterator<char>()));
}

TEST("large union", 1) {
  std::string serialized;
  MEASURE("large intersection", 1s) {
    const TextStat ts1(AUX_DIR + "large_sub1.txt"),
        ts2(AUX_DIR + "large_sub2.txt");
    const TextStat ts = ts1 | ts2;
    CHECK_EQ(1000, ts.size());
    serialized = ts.serialize();
  }
  std::ifstream serialized_ref(AUX_DIR + "large_union.serialized");
  CHECK_EQ(true, serialized_ref.good());
  CHECK_EQ(true, std::equal(serialized.begin(), serialized.end(),
                            std::istream_iterator<char>(serialized_ref),
                            std::istream_iterator<char>()));
}

TEST("large intersection", 1) {
  std::string serialized;
  MEASURE("large union", 1s) {
    const TextStat ts1(AUX_DIR + "large_sub1.txt"),
        ts2(AUX_DIR + "large_sub2.txt");
    const TextStat ts = ts1 & ts2;
    CHECK_EQ(98, ts.size());
    serialized = ts.serialize();
  }
  std::ifstream serialized_ref(AUX_DIR + "large_intersection.serialized");
  CHECK_EQ(true, serialized_ref.good());
  CHECK_EQ(true, std::equal(serialized.begin(), serialized.end(),
                            std::istream_iterator<char>(serialized_ref),
                            std::istream_iterator<char>()));
}

WOODPECKER_TEST_MAIN(-1, -1)
