//
// Created by nagyd on 10/15/2020.
//

#ifndef MULTILEVEL_LIST_MULTILEVEL_LIST_HPP
#define MULTILEVEL_LIST_MULTILEVEL_LIST_HPP

#include <iostream>
#include <exception>
#include <random>

template<typename T = int>
class multilevel_list {
private:
  static const int levels = 100;

  struct Node{
    T val{};
    int levels = 0;
    Node** children = nullptr;
    Node* parent = nullptr;

    Node() = default;
    explicit Node(T x) : val(x) {};
    ~Node() {
      delete [] children;
    }
  };
  Node* cur = nullptr;
  Node* MAX = nullptr;
  Node MIN;

  static void inserter (Node*, std::vector<Node*>&);
  std::vector<Node*> search_level_nbs (T);

public:
  class Iterator {
    friend class multilevel_list;
  private:
    Node *current;

  public:
    using value_type = T;
    using difference_type = std::ptrdiff_t;
    using pointer = T *;
    using reference = T &;
    using iterator_category = std::bidirectional_iterator_tag;

    Iterator() : current{nullptr} {}
    explicit Iterator(Node *node) : current{node} {}

    bool operator!=(const Iterator &) const;
    bool operator==(const Iterator &) const;

    Iterator &operator++();   // Prefix következőre léptetés
    Iterator operator++(int); // Postfix következőre léptetés
    Iterator &operator--();   // Prefix előzőre léptetés
    Iterator operator--(int); // Postfix előzőre léptetés
    T &operator*() const;     // Dereferencia
  };
  Iterator begin() const;
  Iterator end() const;  // utolsó elem utánra mutat
  Iterator last() const; // utolsó elemre mutat
  [[maybe_unused]] Iterator find(T x);

  multilevel_list();
  ~multilevel_list();
  [[nodiscard]] bool isEmpty() const;
  void insert(T);
  void erase(T);
  bool contains(T);
  size_t size();
  template <typename U> friend std::ostream& operator<<(std::ostream&, const multilevel_list<U>&);
  //void print();
};

template <typename T> multilevel_list<T>::multilevel_list() {
  cur = nullptr;
  MIN.levels = levels;
  MIN.children = new Node*[MIN.levels];
  for (int i = 0; i < MIN.levels; i++)
  {
    MIN.children[i] = nullptr;
  }
  MIN.parent = nullptr;
}

template <typename T> multilevel_list<T>::~multilevel_list() {
  if (isEmpty()) {
    return;
  }
  for (cur = MIN.children[0]; cur != nullptr;) {
    Node* tmp = cur;
    cur = cur->children[0];
    delete tmp;
  }
  MAX = nullptr;
}

template <typename T> void multilevel_list<T>::inserter(Node *p, std::vector<Node *>& nbs) {
  p->parent = nbs[0];
  if (nbs[0]->children[0] != nullptr)
  {
    nbs[0]->children[0]->parent = p;
  }
  for(int i = 0; i < p->levels; i++)
  {
    p->children[i] = nbs[i]->children[i];
    nbs[i]->children[i] = p;
  }
}

template <typename T> std::vector<typename multilevel_list<T>::Node*> multilevel_list<T>::search_level_nbs(T x) {
  std::vector<Node*> nbs (levels);
  cur = &MIN;
  int i = levels-1;
  while (i >= 0)
  {
    if(cur->children[i] == nullptr)
    {
      nbs[i] = cur;
      i--;
    } else {
      if(cur->children[i]->val >= x)
      {
        nbs[i] = cur;
        i--;
      }
      else {
        cur = cur->children[i];
      }
    }
  }
  return nbs;
}

template<typename T> void multilevel_list<T>::insert(T x) {
  Node* p = new Node(x);
  std::random_device rd;
  int rand;
  do
  {
    p->levels++;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0,  99);
    rand = dist(mt);
  } while(p->levels <= levels && rand >= 50);

  p->children = new Node*[p->levels];
  std::vector<Node*> nbs = search_level_nbs(x);

  if (nbs[0]->children[0] == nullptr) {
    inserter(p, nbs);
    MAX = p;
  }
  else if(nbs[0]->children[0]->val != x){
    inserter(p, nbs);
  }
}

template<typename T> size_t multilevel_list<T>::size() {
  size_t sum = 0;
  for (cur = MIN.children[0]; cur != nullptr;){
    sum++;
    cur = cur->children[0];
  }
  return sum;
}

template<typename T> bool multilevel_list<T>::isEmpty() const {
  return MIN.children[0] == nullptr;
}

//template<typename T> void multilevel_list<T>::print() {
//  cur = &MIN;
//  while (cur != nullptr)
//  {
//    std::cout << "val: " << cur->val << std::endl;
//    std::cout << "address: " << cur << "\n";
//    std::cout << "parent: \t";
//    if (cur->parent != nullptr)
//    {
//      std::cout << cur->parent << " ";
//    } else {
//      std::cout << "N" << "  ";
//    }
//    std::cout << std::endl;
//    std::cout << "children: \t";
//    for (int i = 0; i < cur->levels; i++) {
//      if (cur->children[i] != nullptr)
//      {
//        std::cout << cur->children[i] << "  ";
//      } else {
//        std::cout << "N" << "  ";
//      }
//    }
//    std::cout << std::endl << std::endl;
//    cur = cur->children[0];
//  }
//}

template<typename T> bool multilevel_list<T>::contains(T x) {
  std::vector<Node*> nbs = search_level_nbs(x);
  if (nbs[0]->children[0] != nullptr){
    if (nbs[0]->children[0]->val == x){
      return true;
    }
  }
  return false;
}

template<typename T> void multilevel_list<T>::erase(T x) {
  std::vector<Node*> nbs = search_level_nbs(x);
  if (nbs[0]->children[0] != nullptr){
    if (nbs[0]->children[0]->val == x){
      cur = nbs[0]->children[0];
      if (cur->children[0] != nullptr){
        cur->children[0]->parent = cur->parent;
      }
      for (int i = 0; i < cur->levels; i++){
        nbs[i]->children[i] = cur->children[i];
      }
      delete cur;
      cur = nullptr;
    }
  }
}

template<typename T>
bool multilevel_list<T>::Iterator::operator!=(const multilevel_list::Iterator &it) const {
  return current != it.current;
}

template<typename T>
bool multilevel_list<T>::Iterator::operator==(const multilevel_list::Iterator &it) const {
  return current == it.current;
}

template<typename T>
typename multilevel_list<T>::Iterator &multilevel_list<T>::Iterator::operator++() {
  current = current->children[0];
  return *this;
}

template<typename T>
typename multilevel_list<T>::Iterator multilevel_list<T>::Iterator::operator++(int) {
  Iterator it(current);
  current = current->children[0];
  return it;
}

template<typename T>
typename multilevel_list<T>::Iterator &multilevel_list<T>::Iterator::operator--() {
  current = current->parent;
  return *this;
}

template<typename T>
typename multilevel_list<T>::Iterator multilevel_list<T>::Iterator::operator--(int) {
  Iterator it(current);
  current = current->parent;
  return it;
}

template<typename T>
T &multilevel_list<T>::Iterator::operator*() const {
  return current->val;
}


template<typename T>
typename multilevel_list<T>::Iterator multilevel_list<T>::begin() const {
  return Iterator(MIN.children[0]);
}

template<typename T>
typename multilevel_list<T>::Iterator multilevel_list<T>::end() const {
  return Iterator(nullptr);
}

template<typename T>
typename multilevel_list<T>::Iterator multilevel_list<T>::last() const {
  return Iterator(MAX);
}

template<typename T>
[[maybe_unused]] typename multilevel_list<T>::Iterator multilevel_list<T>::find(T x) {
  std::vector<Node*> nbs = search_level_nbs(x);
  if (nbs[0]->children[0] != nullptr){
    if (nbs[0]->children[0]->val == x){
      return Iterator(nbs[0]->children[0]);
    }
  }
  return Iterator(nullptr);
}

template<typename T> std::ostream &operator<<(std::ostream& os, const multilevel_list<T>& ml) {
  for (typename multilevel_list<T>::Iterator it = ml.begin(); it != ml.end(); ++it) {
    os << *it << " ";
  }
  return os;
}


#endif //MULTILEVEL_LIST_MULTILEVEL_LIST_HPP
