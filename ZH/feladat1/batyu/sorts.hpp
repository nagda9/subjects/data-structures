#ifndef SORTS_HPP
#define SORTS_HPP

#include <cstddef>
#include <utility>

/// ========== BUBURÉKRENDEZÉS ==========
void bubbleSort(int *a, int n) {
  for (int j = n - 1; j >= 1; --j) {
    for (int i = 0; i <= j - 1; ++i) { // Végigjárja előről hátra a tömböt
      if (a[i] <= a[i + 1]) { // Ha az aktuális elem kisebb, mint a következő
        // skip               // akkor nem történik semmi
      } else { // Ha az aktuális elem nagyobb, mint a következő
        std::swap(a[i], a[i + 1]); // akkor megcseréljük a két elemet
      }
    }
  }
}

/// ========== MAXIMUM KIVÁLASZTÁSOS RENDEZÉS ==========
// A rendező Segédfüggvénye: megkeresi a maximális értékű elemet a megadott
// tartományban (j-ig)
int maxSel(const int *a, int j) {
  int ind = 0; // A maximális értékű elem indexe
  for (int i = 0; i < j; ++i) {
    if (a[i + 1] > a[ind]) {
      ind = i + 1;
    }
  }
  return ind; // Visszatér a maximális értékű elem indexével
}

void maxSort(int *a, int n) {
  for (int j = n - 1; j >= 1; --j) { // Végigjárja hátulról előrefelé a tömböt
    int ind = maxSel(a, j);          // Megkeresi a maximumot az elejétõl j-ig
    std::swap(a[ind], a[j]); // A megtalált maximális elemet kicseréljük az
                             // aktuális hátsó elemmel
  }
}

/// ========== BESZÚRÓ RENDEZÉS ==========
void insertionSort(int *a, int n) {
  for (int j = 0; j <= n - 2; ++j) { // Az 0. indexű elem már rendezett,
    int w = a[j + 1]; // az 1. indexű (j+1) elemtől indulunk, amit eltárolunk
                      // átmenetileg (beszúrandó elem)
    int i = j;
    // Мeɡkеrеsѕük a 'w' heⅼyét a rendeᴢett szakaszban
    while (i >= 0 && a[i] > w) {
      a[i + 1] = a[i]; // Jobbra toljuk a 'w'-nél nagyobb elemeket
      i = i - 1;
    }
    a[i + 1] = w; // Beszúrjuk a helyére az elmentett értéket
  }
}

#endif
