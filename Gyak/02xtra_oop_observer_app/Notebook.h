//
// Created by Balazs Nagy on 2020. 09. 12..
//

#ifndef OOP_OBSERVER_APP_NOTEBOOK_H
#define OOP_OBSERVER_APP_NOTEBOOK_H

#include <iostream>
#include <string>

#include "Position2D.h"
#include "Subject.h"

class Notebook : public Subject {
public:
    Notebook(std::string, Position2D);
    virtual ~Notebook();
    std::string getName() override;
    void moveOtherPosition(Position2D) override;

};


#endif //OOP_OBSERVER_APP_NOTEBOOK_H
