#ifndef HASHTABLE_HPP_INCLUDED
#define HASHTABLE_HPP_INCLUDED

#include <iostream>
#include <list>
#include <vector>

// Exception annak kezelésére, ha túlcsordul a túlcsordulási területünk is
class OverflowError : public std::exception {
public:
  const char *what() const noexcept override {
    return "Elfogyott a tulcsordulasi teruletunk!";
  }
};

// HashTable osztály
// Halmazként működik - minden elemet csak egyszer tárolunk el benne
class HashTable {
  // Belső struktúra, egy elem a hashtáblában
  struct Node {
    int key;   // A tárolt kulcs
    bool used; // Van-e itt hasznos érték?
    int next;  // Következő elem indexe, -1: nincs
    Node() : key(), used(false), next(-1) {}
  };

  const int tablesize;    // Hashtábla mérete (különböző hashek száma)
                          // Nem egyenlő a maximális mérettel!
  const int overflowsize; // Túlcsordulási terület mérete

  int overflowused; // Túlcsordulási területen tárolt elemek száma
  int actualsize;   // Hashtáblában éppen megtalálható elemek száma
                    // Nem egyenlő a kapacitással!
  int nextfree;     // Következő szabad elem helye

  std::vector<Node> table; // A hashtábla tárterülete

public:
  // Konstruktor, a hashtábla kapacitását kell átadni neki
  HashTable(int _tablesize, int _overflowsize)
      : tablesize(_tablesize), overflowsize(_overflowsize), overflowused(0),
        actualsize(0), nextfree(_tablesize),
        table(
            _tablesize +
            _overflowsize) // Létrehozza az adott méretű vektort a hashtáblához
  {}

  // Visszaadja a hashtáblában található elemek számát
  int size() { return actualsize; }

  // Visszaadja, hogy üres-e a hastábla
  bool empty() { return actualsize == 0; }

  // Kitörli az összes elemet a hashtáblából
  void clear() {
    for (int i = 0; i < tablesize + overflowsize; ++i) {
      table[i].used = false;
    }
    actualsize = 0;
    overflowused = 0;
    nextfree = tablesize;
  }

  // Új elemet szúr be a hashtáblába, ha nincs még ilyen elem benne
  // Ha van már ilyen elem, nem csinál semmit
  void insert(const int &key) {
    int pos = getHash(key);
    // 1.) Megnézzük, hogy szabad-e az a hely, ahová tenni akarunk
    if (!table[pos].used) {
      table[pos].key = key;
      table[pos].used = true;
      table[pos].next = -1;
    }
    // 2.) Végigjárjuk a listát, hátha megtaláljuk
    else {
      if (table[pos].key == key) {
        // Már szerepel az elem a táblában, nem szúrjuk be újra
        return;
      }

      while (table[pos].next != -1) {
        pos = table[pos].next;
        if (table[pos].key == key) {
          // Már szerepel az elem a túlcsordulási területen, nem szúrjuk be újra
          return;
        }
      }

      // Ekkor table[pos] után nem jön semmi

      // A túlcsordulási területre akarunk írni, de már nincs rajta több hely
      if (overflowused >= overflowsize) {
        throw OverflowError();
      }

      // Keressünk helyet
      while (table[nextfree].used) {
        nextfree++;
        // Ha a végére értünk
        if (nextfree == tablesize + overflowsize) {
          nextfree = tablesize;
        }
      }
      // table[nextfree] szabad
      table[nextfree].used = true;
      table[nextfree].key = key;
      table[nextfree].next = -1;
      // Beláncoljuk
      table[pos].next = nextfree;
      // Túlcsordulási területen tárolt elemek száma nőtt
      ++overflowused;
    }
    // Tárolt elemek száma nőtt
    ++actualsize;
  }

  // Megkeresi, hogy egy adott elem megtalálható-e a hashtáblában
  bool find(const int &key) {
    int myHash = getHash(key);
    int pos = myHash;
    // Addig megyünk, amíg vagy végére nem értünk a listának, vagy nemlétező
    // elemet találtunk
    while (pos != -1 && table[pos].used) {
      if (table[pos].key == key) {
        // Megtaláltuk
        return true;
      }
      pos = table[pos].next;
    }
    return false;
  }

  // Kitöröl egy adott elemet, ha megtalálható a hashtáblában
  // Ha nem található meg, nem csinál semmit
  void erase(const int &key) {
    int pos = getHash(key);

    int prev = -1;
    while (pos != -1 && table[pos].used) {
      // Megtaláltuk-e az elemet, amit ki kell törölnünk?
      if (table[pos].key == key) {
        // Tábla területén találtuk meg?
        if (prev == -1) {
          // Van utána elem?
          if (table[pos].next != -1) {
            int source = table[pos].next;
            // Listánk első elemét odahelyezzük
            table[pos] = table[source];
            // Az áthelyezet elemet kitöröljük
            table[source].used = false;
            // Ami a túlcsordulási területen volt, tehát levonjuk
            --overflowused;
          } else {
            // Kitöröljük a jelenlegit
            table[pos].used = false;
          }
        } else // Túlcsordulási területen találtuk meg
        {
          // Kitöröljük a jelenlegit
          table[pos].used = false;
          // Átláncoljuk
          table[prev].next = table[pos].next;
          --overflowused;
        }
        --actualsize;
        return;
      }
      prev = pos;
      pos = table[pos].next;
    }
  }

  // Kiírja a hashtábla tartalmát
  // Soronként egy hashérték, a táblában levő kulcs, majd ha van, akkor nyíllal
  // a következő elem indexe Vonallal elválasztva pedig utána a túlcsordulási
  // terület
  void print() {
    for (int i = 0; i < tablesize; ++i) {
      std::cout << i << ": ";
      if (table[i].used) {
        std::cout << table[i].key;
        if (table[i].next != -1) {
          std::cout << " -> *" << table[i].next;
        }
      }
      std::cout << std::endl;
    }
    std::cout << "-------" << std::endl;
    for (int i = tablesize; i < tablesize + overflowsize; ++i) {
      std::cout << i << ": ";
      if (table[i].used) {
        std::cout << table[i].key;
        if (table[i].next != -1) {
          std::cout << " -> *" << table[i].next;
        }
      }
      std::cout << std::endl;
    }
  }

private:
  // Visszaadja egy adott kulcs hashét
  int getHash(const int &key) {
    // Egyszerű hashfüggvény: csak visszaadja a számot modulózva a kapacitással
    return key % tablesize;
  }
};

#endif // HASHTABLE_HPP_INCLUDED
