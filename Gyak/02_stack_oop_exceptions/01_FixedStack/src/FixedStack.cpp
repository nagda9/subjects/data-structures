#include "FixedStack.h"

#include "exceptions.hpp"

FixedStack::FixedStack() {
	head = 0;
}

FixedStack::~FixedStack() {

}

bool FixedStack::isEmpty() const {
	return (head == 0);
}

void FixedStack::push(int new_item) {
	if (head == max) {
		throw OverFlowException();
	}

	array[head] = new_item;
	head++;
}

int FixedStack::top() const {
	if (isEmpty()) {
		throw UnderFlowException();
	}

	return array[head - 1];
}

int FixedStack::pop() {
	if (isEmpty()) {
		throw UnderFlowException();
	}

	int tmp = array[head - 1];
	head--;
	return tmp;
}

void FixedStack::print() const {
	for (int i = 0; i < head - 1; i++) {
		std::cout << array[i] << ", ";
	}
	if (!isEmpty()) {
		std::cout << array[head - 1] << std::endl;
	}
}
