#ifndef HF02_EXCEPTIONS_HPP
#define HF02_EXCEPTIONS_HPP

#include <iostream>
#include <exception>

class IndependentEdge : public std::exception {
    std::string message;
public:
    IndependentEdge(const std::string& from, const std::string& to, const int& val) {
        message = "Could not create " + from + " - " + to + " with value " + std::to_string(val) + ", because it is independent from the other vertices!";
    }

    [[nodiscard]] const char* what() const noexcept override {
        return message.c_str();
    }
};

class IndependentVertex : public std::exception {
    std::string message;
public:
    explicit IndependentVertex(const std::string& vertex) {
        message = "Not allowed operation! Inserting" + vertex + "makes it independent.";
    }
    explicit IndependentVertex(const std::string& from, const std::string& to) {
        message = "Not allowed operation! Deleting edge " + from + "--" + to + " makes a vertex independent.";
    }

    [[nodiscard]] const char* what() const noexcept override {
        return message.c_str();
    }
};

#endif //HF02_EXCEPTIONS_HPP
