#include <iostream>
#include <vector>

using namespace std;

class Parent{
    public:
        // virtualis destructor
        // mi torténik ha a virtual szo oda van irva? és ha nincs?

        virtual ~Parent(){ cout << "Virtualis destructor meghivodott! "; }
        //~Parent(){ cout << "Parent destructora meghivodott! "; }
};

class Child : public Parent{
    public:
    ~Child() {
        cout << "Child destructora meghivodott! ";
    }
};

int main()
{
    Parent* c = new Child();
    delete c;
    return 0;
}
