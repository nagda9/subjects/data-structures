#ifndef BASE2_H
#define BASE2_H

#include <string>

class Base2 {
public:
	Base2();
	virtual ~Base2();
	//Base 1 osztalyban is van ilyen nevu fuggveny!
	void f();

	void printB();
protected:
	std::string b;
private:
};

#endif // BASE2_H
