#ifndef TIMER_HPP_INCLUDED
#define TIMER_HPP_INCLUDED
#include <chrono>

class Timer {
  std::chrono::high_resolution_clock::time_point begin;

public:
  Timer() : begin(std::chrono::high_resolution_clock::now()) {}

  void start() { begin = std::chrono::high_resolution_clock::now(); }

  double stop() {
    auto end = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::duration<double>>(end -
                                                                     begin)
        .count();
  }
};

#endif // TIMER_HPP_INCLUDED