#include "DynamicIntQueue.h"
#include "../exceptions/exceptions.hpp"

#include <iostream>

DynamicIntQueue::DynamicIntQueue(const DynamicIntQueue &other) : head{nullptr}, tail{nullptr} {
  for (Node *node = other.head; node != nullptr; node = node->pNext) {
    in(node->value);
  }
}

DynamicIntQueue::DynamicIntQueue(DynamicIntQueue &&other) : head{other.head}, tail{other.tail} {
  other.head = other.tail = nullptr;
}

DynamicIntQueue &DynamicIntQueue::operator=(DynamicIntQueue &rhs) {
  if (&rhs != this) {
    // RAII -- copy ctor
    DynamicIntQueue temp{rhs};
    temp.swap(*this);
    // temp destruktora elintézi a mi régi implementációnkat
  }

  return *this;
}

DynamicIntQueue &DynamicIntQueue::operator=(DynamicIntQueue &&rhs) {
  swap(rhs);
  return *this;
}

DynamicIntQueue::~DynamicIntQueue() {
  while (!isEmpty()) {
    out();
  }
}

void DynamicIntQueue::swap(DynamicIntQueue &other) noexcept {
  std::swap(this->head, other.head);
  std::swap(this->tail, other.tail);
}

bool DynamicIntQueue::isEmpty() const {
  // Miért nem használhatjuk a `tail`-t erre a célra?
  // Mi a `tail` értéke, ha volt már elem a sorban, de az összeset kivettük?
  return head == nullptr;
}

void DynamicIntQueue::in(int new_item) {
  Node *p = new Node(new_item);
  if (isEmpty()) {
    tail = p;
    head = p;
  } else {
    tail->pNext = p;
    tail = p;
  }
}

int DynamicIntQueue::out() {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  int tmp = head->value;
  Node *p = head;
  head = head->pNext;
  delete p;
  return tmp;
}

int DynamicIntQueue::first() const {
  if (isEmpty()) {
    throw UnderFlowException();
  }

  return head->value;
}

void DynamicIntQueue::print() const {
  for (Node *i = head; i != nullptr; i = i->pNext) {
    std::cout << i->value << " ";
  }
}
