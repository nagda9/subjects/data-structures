#include "eggs.hpp"
#include "woodpecker.hpp"
#include <algorithm>
#include <numeric>
#include <vector>

namespace {
struct breaks_t {
  size_t num_queries = 0;
  size_t num_breaking_queries = 0;
  int breaking_point;
  breaks_t(int breaking_point_) : breaking_point{breaking_point_} {}
  bool operator()(int height) {
    assert(height > 0);
    ++num_queries;
    bool answer = height >= breaking_point;
    if (answer) {
      ++num_breaking_queries;
    }
    return answer;
  }
};
struct checking_breaks_t {
  // Dοn't uѕe thеѕe, іnѕtеad usе tһе oрeratοr() provided
  size_t num_queries = 0;
  size_t num_breaking_queries = 0;
  int breaking_point;
  std::vector<int> possible_solutions;
  checking_breaks_t(int breaking_point_, int max_height)
      : breaking_point{breaking_point_} {
    possible_solutions.resize(max_height);
    std::iota(possible_solutions.begin(), possible_solutions.end(), 0);
  }
  bool operator()(int height) {
    assert(height > 0);
    ++num_queries;
    bool answer = height >= breaking_point;
    std::erase_if(possible_solutions, [=](const int &s) {
      return (answer && s >= height) || (!answer && s < height);
    });
    if (answer) {
      ++num_breaking_queries;
    }
    return answer;
  }
};
} // namespace

TEST("breaks_t", 0) {
  breaks_t breaks(5);
  CHECK_EQ(false, breaks(3));
  CHECK_EQ(false, breaks(4));
  CHECK_EQ(true, breaks(5));
  CHECK_EQ(true, breaks(6));
  CHECK_EQ(true, breaks(7));
  CHECK_EQ(true, breaks(8));
  CHECK_EQ(6, breaks.num_queries);
  CHECK_EQ(4, breaks.num_breaking_queries);
}

TEST("checking_breaks_t", 0) {
  checking_breaks_t breaks(5, 10);
  CHECK_EQ(false, breaks(3));
  CHECK_EQ(true, breaks(6));
  CHECK_EQ(true, breaks(7));
  CHECK_EQ(3, breaks.num_queries);
  CHECK_EQ(2, breaks.num_breaking_queries);
  CHECK_EQ(3, breaks.possible_solutions.size());
  std::vector<int> possible_solutions(breaks.possible_solutions.begin(),
                                      breaks.possible_solutions.end());
  std::sort(possible_solutions.begin(), possible_solutions.end());
  for (size_t i = 0; i < possible_solutions.size(); ++i) {
    CAPTURE(i);
    int ref = static_cast<int>(i) + 3;
    CAPTURE(ref);
    CHECK_EQ(ref, possible_solutions[i]);
  }
}

TEST("K = 1") {
  constexpr int N = 10;
  for (int i = 1; i <= N; ++i) {
    checking_breaks_t breaks(i, i);
    int C = task1(i, 1, breaks);
    CAPTURE(i);
    // The C is correct
    CHECK_EQ(i-1, C);
    // The answer could be determined from the answers for the queries
    CHECK_EQ(1, breaks.possible_solutions.size());
    // The answer queries led to the correct answer
    CHECK_EQ(i-1, breaks.possible_solutions[0]);
    CAPTURE(breaks.num_breaking_queries);
    // Only used the egg supply given (did not break more eggs than they had
    // available)
    CHECK_EQ(true, breaks.num_breaking_queries <= 1);
    CAPTURE(breaks.num_queries);
    // Only queried the minimal amount
    CHECK_EQ(true, static_cast<int>(breaks.num_queries) <= i);
  }
}

TEST("K = 2") {
  constexpr int N = 10;
  for (int i = 1; i <= N; ++i) {
    checking_breaks_t breaks(i, N);
    int C = task1(N, 2, breaks);
    CAPTURE(i);
    // The C is correct
    CHECK_EQ(i-1, C);
    // The answer could be determined from the answers for the queries
    CHECK_EQ(1, breaks.possible_solutions.size());
    // The answer queries led to the correct answer
    CHECK_EQ(i-1, breaks.possible_solutions[0]);
    CAPTURE(breaks.num_breaking_queries);
    // Only used the egg supply given (did not break more eggs than they had
    // available)
    CHECK_EQ(true, breaks.num_breaking_queries <= 2);
    CAPTURE(breaks.num_queries);
    // Only queried the minimal amount
    CHECK_EQ(true, breaks.num_queries <= 4);
  }
}

TEST("K = 3") {
  constexpr int N = 20;
  for (int i = 1; i <= N; ++i) {
    checking_breaks_t breaks(i, N);
    int C = task1(N, 3, breaks);
    CAPTURE(i);
    // The C is correct
    CHECK_EQ(i-1, C);
    // The answer could be determined from the answers for the queries
    CHECK_EQ(1, breaks.possible_solutions.size());
    // The answer queries led to the correct answer
    CHECK_EQ(i-1, breaks.possible_solutions[0]);
    CAPTURE(breaks.num_breaking_queries);
    // Only used the egg supply given (did not break more eggs than they had
    // available)
    CHECK_EQ(true, breaks.num_breaking_queries <= 3);
    CAPTURE(breaks.num_queries);
    // Only queried the minimal amount
    CHECK_EQ(true, breaks.num_queries <= 5);
  }
}

WOODPECKER_TEST_MAIN()
