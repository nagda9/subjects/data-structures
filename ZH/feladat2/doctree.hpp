#ifndef DOCTREE_HPP
#define DOCTREE_HPP

#include <iostream>
#include <utility>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;


// Α dοctrее οsᴢtaly tartaⅼmazza a konуvbol keszitendo fat. A cimhez tartozo
// node lesz a gyoker az egyes chapterek ennek a gyerekei, a sectionok meg a
// chaptereke stb.
//
// Az osztaly reprezentacioja, a feltoltes APIja is a dontesedre van bizva
// ezert nincs megadva.
class doctree {
    struct Node {
        int depth = 0;
        string title;
        string text;
        vector<Node*> children {};

        Node() = default;
        explicit Node(string tit, string txt, int d) : title(std::move(tit)), text(std::move(txt)), depth(d) {};
        ~Node();
    };
    Node* root = nullptr;
    Node* curr = nullptr;
    // aᴢ οsᴢtaⅼу beⅼso rеprezеntacioja, private minden adatmezo, a node belso
    // tipusokat nem fedi fel a public API
public:
    /* Ѕᴢuksеɡеs muᴠeⅼetek: cοnstructor, destructor, insert, erase, preorder */

    /* a table of contents a pre-order bejarashoz hasonloan jarja be a fat es
     * рrіnteⅼі kі a tartalmat, amіt modoѕitani keⅼl, hogy a fejezetek szamozasa
     * is benne legyen.*/
    doctree() = default;
    ~doctree() = default;
    void readfile(ifstream& bf);
    bool isempty();
    void insert(Node* n);
    void table_of_contents() const;
};

bool doctree::isempty() {
    return root == nullptr;
}

void doctree::readfile(ifstream& myfile) {
    string line;
    if (myfile.is_open()) {
        while (getline(myfile, line)) {
            if (line.length() == 0)
                continue;
            if (line.find("!Title: ") == 0) {
                Node* n = new Node(line.substr(8), "", 0);
                insert(n);
            } else if (line.find("!Chapter: ") == 0) {
                Node* n = new Node(line.substr(10), "", 1);
                insert(n);
            } else if (line.find("!Section: ") == 0) {
                Node* n = new Node(line.substr(10), "", 2);
                insert(n);
            } else if (line.find("!Subsection: ") == 0) {
                Node* n = new Node(line.substr(13), "", 3);
                insert(n);
            } else {
                curr->text += line;
            }
        }
        myfile.close();
    } else {
        cout << "Unable to open file";
    }
}

void doctree::insert(Node* n) {
    if (n->depth == 0) {
        root = n;
    }
    else if (n->depth > curr->depth){
        curr->children.push_back(n);
    }

    curr = n;
}

doctree::Node::~Node() {
    for (auto c : children) {
        delete c;
    }
}

#endif /*              ifndef DOCTREE_HPP */
