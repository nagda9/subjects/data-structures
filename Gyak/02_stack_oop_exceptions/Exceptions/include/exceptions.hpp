#ifndef EXCEPTIONS_HPP_INCLUDED
#define EXCEPTIONS_HPP_INCLUDED

#include <exception>

class UnderFlowException : public std::exception {
    public:
        const char* what() const noexcept { return "Alulcsordulas!"; }
        // pre C++11, deprecated, removed in C++20
        // const char* what() const throw() { return "Alulcsordulas!"; }
};

class OverFlowException : public std::exception {
    public:
        const char* what() const noexcept { return "Tulcsordulas!"; }
        // pre C++11, deprecated, removed in C++20
        // const char* what() const throw() { return "Tulcsordulas!"; }
};

#endif // EXCEPTIONS_HPP_INCLUDED
