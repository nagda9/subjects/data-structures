//
// Created by nagyd on 2020. 10. 06..
//

#ifndef KHF02_PLAN_HPP
#define KHF02_PLAN_HPP

#include <iostream>
#include <vector>
#include <set>
#include <exception>
#include <chrono>
#include <algorithm>

using namespace std;

struct Corridor {
    int32_t from, to;

    Corridor() = default;

    Corridor(int32_t _from, int32_t _to) : from(_from), to(_to) {};

    friend ostream& operator<<(ostream& os, const Corridor& c) {
        os << c.from << " - " << c.to;
        return os;
    }

    bool operator<(const Corridor& other) const {
        if (from < other.from) {
            return true;
        } else if (from == other.from) {
            return to < other.to;
        } else {
            return false;
        }
    }

    bool operator==(const int32_t& x) const {
        if (from == x || to == x) {
            return true;
        }
        return false;
    }
};

struct Node {
    int32_t num = -1;
    vector<int32_t> loop{};
    vector<Node*> children{};
    Node* parent = nullptr;

    friend ostream& operator<<(ostream& os, const Node& n) {
        os << n.num << endl;
        os << "parent: ";
        if (n.parent != nullptr) {
            os << n.parent->num << endl;
        } else {
            os << " - " << endl;
        }

        os << "children: ";
        if (n.children.empty()) {
            os << " - " << endl;
        }
        for (auto c : n.children) {
            if (c == n.children.back()) {
                os << c->num << endl;
            } else {
                os << c->num << ", ";
            }
        }

        os << "loop: ";
        if (n.loop.empty()) {
            os << " - " << endl;
        }
        for (auto l : n.loop) {
            if (l == n.loop.back()) {
                os << l << endl;
            } else {
                os << l << ", ";
            }
        }
        os << endl;
        return os;
    }
};

bool checkParent(const Node& n) {
    return n.parent != nullptr;
}

[[maybe_unused]] void print(const vector<Node>& array) {
    for (const auto& i : array) {
        cout << i;
    }
}

void followChildren(vector<Node>& array, vector<int32_t>& result, int32_t node) {
    if (node != 0) {
        result.push_back(node);
    }

    if (!array[node].loop.empty()) {
        for (auto l : array[node].loop) {
            result.push_back(l);
            result.push_back(node);
        }
    }

    if (!array[node].children.empty()) {
        for (auto c : array[node].children) {
            followChildren(array, result, c->num);
        }
    }
    if (node != 0) {
        if (array[node].parent->num != 0) {
            result.push_back(array[node].parent->num);
        }
    }
}

int32_t moveMarvin(vector<Node>& array, vector<int32_t>& result, int32_t node) {
    int32_t next;
    if (node != 0) {
        result.push_back(node);
    }

    if (!array[node].loop.empty()) {
        for (auto l : array[node].loop) {
            result.push_back(l);
            result.push_back(node);
        }
        array[node].loop.clear();
    }


    if (!array[node].children.empty()) {
        next = array[node].children.back()->num;
        array[node].children.pop_back();
    } else {
        next = array[node].parent->num;
    }
    return next;
}

////////////////////////////////////////////////////////
template<typename Iterator>
vector<int32_t> plan(Iterator corridors_begin, Iterator corridors_end, size_t N) {
    auto t1 = std::chrono::high_resolution_clock::now();
    cout << "called" << endl;
    vector<Corridor> edges;
    vector<Node> array(N + 1);
    vector<int32_t> result{};
    copy(corridors_begin, corridors_end, back_inserter(edges));

    for (auto it = edges.begin(); it != edges.end(); it++) {
        if (it == edges.begin()) {
            array[0].num = 0;
            array[it->from].num = it->from;
            array[it->to].num = it->to;

            array[0].children.push_back(&array[it->from]);
            array[it->from].parent = &array[0];

            array[it->from].children.push_back(&array[it->to]);
            array[it->to].parent = &array[it->from];
        } else {
            array[it->from].num = it->from;
            array[it->to].num = it->to;

            pair<bool, bool> P;
            P.first = checkParent(array[it->from]);
            P.second = checkParent(array[it->to]);

            if ((P.first == 1 && P.second == 0) || (P.first == 0 && P.second == 0)) {
                array[it->from].children.push_back(&array[it->to]);
                array[it->to].parent = &array[it->from];
            } else if (P.first == 0 && P.second == 1) {
                array[it->to].children.push_back(&array[it->from]);
                array[it->from].parent = &array[it->to];
            } else if (P.first == 1 && P.second == 1) {
                array[it->from].loop.push_back(it->to);
            }
        }
    }

    cout << "generated" << endl;
    int32_t current = 0;
    int32_t next = -1;
    while (next != 0) {
        next = moveMarvin(array, result, current);
        current = next;
    }
    //followChildren(array, result, 0);

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    cout << "duration: " << duration << endl;

    return result;
}

#endif //KHF02_PLAN_HPP
