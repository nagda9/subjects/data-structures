//
// Created by nagyd on 10/22/2020.
//

#ifndef KHF03_BINEXPTREE_HPP
#define KHF03_BINEXPTREE_HPP

#include <iostream>
#include <vector>
#include <memory>
#include <exception>
#include <set>
#include <stack>

using namespace std;

class BinExpTree{
private:
  [[maybe_unused]] static const set<string> opers;
  vector<string> postfix;

  friend ostream& operator<< (ostream&, const BinExpTree&);
public:

  explicit BinExpTree(const string& expression);

  [[nodiscard]] static vector<string> parseExpression(const string& expression);
  [[nodiscard]] string printExpression() const;
  double evaluateExpression();
};

[[maybe_unused]] const set<string> BinExpTree::opers {"+","-","/","*"};

BinExpTree::BinExpTree(const string& expression) {
  postfix = parseExpression(expression);
}

vector<string> BinExpTree::parseExpression(const string &expression) {
  vector<string> elements {};
  stack<string> sds {};
  string str;
  for (auto e : expression)
  {
    string tmp = string(1, e);

    if (tmp == "(")
    {
      sds.push(tmp);
    }
    else if (opers.find(tmp) != opers.end()) {
      if (!str.empty())
      {
        elements.emplace_back(str);
        str = "";
      }

      if ((tmp == "-" || tmp == "+") && !sds.empty()) {
        while (sds.top() == "*" || sds.top() == "/") {
          elements.emplace_back(sds.top());
          sds.pop();
        }
      }
      sds.push(tmp);
    }
    else if (tmp == string(1, expression.back()) && tmp != ")") {
      str += tmp;
      elements.emplace_back(str);
      str = "";

      while (!sds.empty())
      {
        elements.push_back(sds.top());
        sds.pop();
      }
    }
    else if (tmp == ")") {
      elements.emplace_back(str);
      str = "";

      while (!sds.empty() && sds.top() != "("){
        elements.emplace_back(sds.top());
        sds.pop();
      }
      if (!sds.empty()) {
        sds.pop();
      }
      if (tmp == string(1, expression.back()))
      {
        while (!sds.empty())
        {
          elements.emplace_back(sds.top());
          sds.pop();
        }
      }
    }
    else {
      str += tmp;
    }
  }

  return elements;
}

ostream &operator<<(ostream& os, const BinExpTree& B) {
  for (const auto& e : B.postfix)
  {
    os << e << endl;
  }
  return os;
}

string BinExpTree::printExpression() const {
  stack<string> s;

  for (const auto& e : postfix)
  {
    if (opers.find(e) == opers.end())
    {
      s.push(e);
    }
    else {
      string after = s.top();
      s.pop();
      string before = s.top();
      s.pop();
      if (e == "*" || e == "/")
      {
        string prev;
        string next;
        size_t i = 0;
        while (i < before.size()){
          if (opers.find(string(1, before[before.size()-i])) != opers.end())
          {
            prev = before[before.size()-i];
            break;
          }
          i++;
        }
        i = 0;
        while (i < after.size()){
          if (opers.find(string(1, after[i])) != opers.end())
          {
            next = after[i];
            break;
          }
          i++;
        }

        if (prev == "+" || prev == "-") {
          before = "(" + before + ")";
        }
        if (next == "+" || next == "-") {
          after = "(" + after + ")";
        }
      }
      string result = before + e + after;
      s.push(result);
    }
  }
  return s.top();
}

double BinExpTree::evaluateExpression() {
  stack<double> s;

  for(const auto& e : postfix) {
    if (opers.find(e) == opers.end()){
      double x = stod(e);
      s.push(x);
    }
    else {
      double after = s.top();
      s.pop();
      double before = s.top();
      s.pop();

      double result;
      if (e == "+") {
        result = before + after;
      }
      else if (e == "-") {
        result = before - after;
      }
      else if (e == "*") {
        result = before * after;
      }
      else if (e == "/") {
        result = before / after;
      }
      s.push(result);
    }
  }
  return s.top();
}

#endif //KHF03_BINEXPTREE_HPP