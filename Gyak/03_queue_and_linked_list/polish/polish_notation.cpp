#include "DynamicQueue.h"
#include "DynamicStack.h"
#include <iostream>
#include <stdio.h>

using namespace std;

bool priority(char operator_) {
  if (operator_ == '/' || operator_ == '*')
    return 1;
  else
    return 0;
}

int main() {
  DynamicQueue<char> original_queue;
  DynamicQueue<char> result_queue;
  DynamicStack<char> stack;

  cout << "Polish Notation" << endl;

  string expression;
  cout << "\nPlease give the original form of the expression:" << endl;
  cin >> expression;

  for (unsigned int i = 0; i < expression.size(); i++) {
    original_queue.in(expression[i]);
  }

  cout << "So the original expression: ";
  original_queue.print();
  cout << endl << endl;

  while (!original_queue.isEmpty()) {
    char c = original_queue.out();

    if (c == '(') {
      stack.push(c);
    } else if (c == ')') {
      while (stack.top() != '(') {
        result_queue.in(stack.pop());
      }
      stack.pop();
    } else if (c == '+' || c == '-' || c == '*' || c == '/') {

      while (!stack.isEmpty() && stack.top() != '(' &&
             ((priority(stack.top()) > priority(c)) ||
              (priority(stack.top()) == priority(c)))) {
        result_queue.in(stack.pop());
      }

      stack.push(c);
    } else { // if number
      result_queue.in(c);
    }
  }
  while (!stack.isEmpty()) {
    result_queue.in(stack.pop());
  }

  cout << "And then the result postfix polish nomination: ";
  result_queue.print();
  cout << endl << endl;

  return 0;
}
