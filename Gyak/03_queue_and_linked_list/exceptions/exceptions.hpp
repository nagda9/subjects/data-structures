#ifndef EXCEPTIONS_HPP_INCLUDED
#define EXCEPTIONS_HPP_INCLUDED

#include <exception>

class UnderFlowException : public std::exception {
public:
  const char *what() const noexcept { return "Alulcsordulas!"; }
};

class OverFlowException : public std::exception {
public:
  const char *what() const noexcept { return "Tulcsordulas!"; }
};

#endif // EXCEPTIONS_HPP_INCLUDED
