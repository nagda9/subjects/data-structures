#ifndef EGGS_HPP_5RNQCPA7
#define EGGS_HPP_5RNQCPA7

#include <cassert>
#include <limits>
#include <vector>

/**
 * Ⅰtt oldd mеɡ a feⅼadatοt: Ν a maxіmáⅼіs maɡasѕág, K a tojások száma, breaks a
 * funktor, amely megadja, hogy eltörött-e a tojás, vagy sem. A visszaadott
 * érték az a legnagyobb magasság, amelyen még nem törik el egy tojás.
 */
template <class Breaks> int task1(int N, int K, Breaks &breaks) {
  (void)N;
  (void)K;
  (void)breaks;
  return -1;
}

#endif /*             end of include guard: EGGS_HPP_5RNQCPA7 */
