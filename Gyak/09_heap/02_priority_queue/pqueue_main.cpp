/**
 *
 * @file pqueue_main.cpp
 * @author tekda
 * @author horar
 * Főprogram prioritásos sor demonstrálására
 *
 */

#include "pqueue.hpp"
#include <iostream>
#include <random>

/**
 * Spéci típus a kupac tesztelésére. Csak összehasonlító operátora van.
 */
struct DummyType {
  int a;
  bool operator<(DummyType rhs) const { return this->a < rhs.a; }
};

int main() {
  // Reproducibilis mérések
  std::mt19937 gen(0); // NOLINT: Szandekos konstans seed
  std::uniform_int_distribution<int> dist(0, 100);
  try {
    {
      std::cout << std::endl << "-----------------------------" << std::endl;
      std::cout << "Prioritasos sor";
      std::cout << std::endl << "-----------------------------" << std::endl;

      priority_queue<int> pq;
      std::cout << std::endl;
      std::cout << "Beillesztunk elemeket: ";

      for (int i = 0; i < 30; ++i) {
        int val = dist(gen);
        std::cout << val << " ";
        pq.push(val,
                val); // itt most teszteljük, hogy a priority és a value
                      // ugyanaz pq.push(val, dist(gen));
      }
      std::cout << std::endl
                << (pq.validate() ? "Rendben van!" : "Valami nem oke!")
                << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << "Kiveszunk egy par elemet: ";
      for (int i = 0; i < 5; ++i) {
        std::cout << pq.top() << " ";
        pq.pop();
      }
      std::cout << std::endl
                << (pq.validate() ? "Rendben van!" : "Valami nem oke!")
                << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << "A sorban maradt elemek: ";
      while (!pq.empty()) {
        std::cout << pq.top() << " ";
        pq.pop();
      }
      std::cout << std::endl
                << (pq.validate() ? "Rendben van!" : "Valami nem oke!")
                << std::endl;
    }

    std::cout << std::endl
              << "----------------------------------------------------------"
              << std::endl;
    std::cout << "Veget ert a program futasa, ha minden rendben van, akkor "
                 "minden rendben van!";
    std::cout << std::endl
              << "----------------------------------------------------------"
              << std::endl;

  } catch (heap_exception &e) {
    std::cout << "HIBA: " << e.what() << std::endl;
  }

  return 0;
}
