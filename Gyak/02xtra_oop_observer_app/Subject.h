#ifndef SUBJECT_H
#define SUBJECT_H

#include <vector>
#include <algorithm>

#include "BaseStation.h"
#include "Position2D.h"

class Subject
{
    public:
        Subject();
        virtual ~Subject();

        void attach(BaseStation*);
        void notify(Position2D);
        void detach(BaseStation*);
        virtual void moveOtherPosition(Position2D) = 0;

        virtual std::string getName() = 0;
        Position2D getPos();

protected:
        std::string name;
        Position2D pos;

    private:
        std::vector<BaseStation*> stations;
};

#endif // SUBJECT_H
