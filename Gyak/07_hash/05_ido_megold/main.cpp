#include "Timer.hpp"
#include "hashtable.hpp"
#include <iostream>
#include <random>
#include <set>
#include <unordered_set>

int main() {
  std::mt19937 gen(std::random_device{}());
  std::uniform_int_distribution<int> dist;

  /// Idő teszt
  int large_test_num = 1000000;
  std::cout << std::endl << "== Ido tesztek ==" << std::endl;

  // std::set
  std::cout << "std::set (O(log(n))):" << std::endl;
  Timer set_timer;
  std::set<int> set_large;
  for (int i = 0; i < large_test_num; ++i) {
    set_large.insert(dist(gen));
  }
  for (int i = 0; i < large_test_num; ++i) {
    set_large.find(dist(gen));
  }
  for (int i = 0; i < large_test_num; ++i) {
    set_large.erase(dist(gen));
  }
  set_large.clear();
  std::cout << set_timer.stop() << " sec" << std::endl;

  // hashtábla
  std::cout << "sajat hashtabla (O(1)):" << std::endl;
  Timer hash_timer;
  try {
    // Érdemes megnézni
    HashTable<int, IntFingerprint> hashtable_large(
        10 * large_test_num); // Próbáld változtatni
    for (int i = 0; i < large_test_num; ++i) {
      hashtable_large.insert(dist(gen));
    }
    for (int i = 0; i < large_test_num; ++i) {
      hashtable_large.find(dist(gen));
    }
    for (int i = 0; i < large_test_num; ++i) {
      hashtable_large.erase(dist(gen));
    }
    hashtable_large.clear();
  } catch (OverflowError &) {
    std::cout << "Megtelt a hashtábla..." << std::endl;
  }
  std::cout << hash_timer.stop() << " sec" << std::endl;

  // std::unordered_set
  // C++11-et igényel!
  std::cout << "std::unordered_set (O(1)):" << std::endl;
  Timer unordered_timer;
  std::unordered_set<int> unordered_set_large;
  for (int i = 0; i < large_test_num; ++i) {
    unordered_set_large.insert(dist(gen));
  }
  for (int i = 0; i < large_test_num; ++i) {
    unordered_set_large.find(dist(gen));
  }
  for (int i = 0; i < large_test_num; ++i) {
    unordered_set_large.erase(dist(gen));
  }
  unordered_set_large.clear();
  std::cout << unordered_timer.stop() << " sec" << std::endl;

  return 0;
}
