//
// Created by Balazs Nagy on 2020. 09. 12..
//

#include "Notebook.h"

Notebook::Notebook(std::string _name, Position2D _pos)
{
    name = _name;
    pos = _pos;
}

Notebook::~Notebook()
{

}

std::string Notebook::getName() {
    return name;
}

void Notebook::moveOtherPosition(Position2D _pos) {
    std::cout << name << " is moving to position " << _pos.x << " " << _pos.y << "\n";
    std::cout << "I am a notebook...\n";
    this->pos = _pos;
    notify(_pos);
}
