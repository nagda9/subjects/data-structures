//
// Created by Balazs Nagy on 2020. 09. 12..
//

#ifndef OOP_OBSERVER_APP_APPLICATION_H
#define OOP_OBSERVER_APP_APPLICATION_H

#include "Layer.h"

#include <memory>
#include <vector>
#include <string>

namespace app {

    class Application {
        std::string app_name = "Observer App";
        std::vector<Layer*> layers;

    public:
        Application(const std::string&);
        void attach_layer(Layer*);
        void run();
    };
}


#endif //OOP_OBSERVER_APP_APPLICATION_H
