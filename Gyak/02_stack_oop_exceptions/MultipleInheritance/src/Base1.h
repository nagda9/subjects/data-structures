#ifndef BASE1_H
#define BASE1_H

#include <string>

class Base1 {
public:
	Base1();
	virtual ~Base1();

	//Base 1 osztalyban is van ilyen nevu fuggveny!
	void f();

	void printA();
protected:
	std::string a;
private:
};

#endif // BASE1_H
