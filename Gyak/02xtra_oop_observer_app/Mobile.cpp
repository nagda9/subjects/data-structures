#include "Mobile.h"

Mobile::Mobile(std::string _name, Position2D _pos)
{
    name = _name;
    pos = _pos;
}

Mobile::~Mobile()
{

}

std::string Mobile::getName() {
    return name;
}

void Mobile::moveOtherPosition(Position2D _pos) {
    std::cout << name << " is moving to position " << _pos.x << " " << _pos.y << "\n";
    std::cout << "I am a mobile...\n";
    this->pos = _pos;
    notify(_pos);
}