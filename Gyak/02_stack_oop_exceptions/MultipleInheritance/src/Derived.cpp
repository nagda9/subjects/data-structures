#include "Derived.h"
#include <iostream>

Derived::Derived() {
	std::cout << "Constructor Derived ()" << std::endl;
}

Derived::~Derived() {
	std::cout << "Destructor ~Derived ()" << std::endl;
}

void Derived::printAB() {
	std::cout << a << " " << b << std::endl;
}
